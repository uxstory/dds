package com.uxstory.dds.speedplus.activity.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.res.ResourcesCompat;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.config.Constants;
import com.uxstory.dds.speedplus.util.LogU;
import com.uxstory.dds.speedplus.util.ScaleUtil;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ViewUtil {

    private static final String TAG = ViewUtil.class.getName();

    private static final String HIGHLIGHT_START_TAG = "\\{#";
    private static final String HIGHLIGHT_END_TAG = "#\\}";
    private static final String HIGHLIGHT_PATTERN = HIGHLIGHT_START_TAG + "((?!" + HIGHLIGHT_END_TAG + ").)+" + HIGHLIGHT_END_TAG;

    private static final String LINK_START_TAG = "\\{@";
    private static final String LINK_END_TAG = "@\\}";
    private static final String LINK_PATTERN = LINK_START_TAG + ".+" + LINK_END_TAG;

    private static final String EMAIL_PATTERN = "[\\w\\~\\-\\.]+@[\\w\\~\\-]+(\\.[\\w\\~\\-]+)+";


    public static void highlightTagText(Context context, TextView textView, String fullStr, int highRightColorId) {
        highlightTagText(context, textView, fullStr, null, highRightColorId);
    }

    public static void highlightTagText(Context context, TextView textView, String fullStr, String searchStr, int highRightColorId) {

        SpannableStringBuilder ssb = new SpannableStringBuilder();
        highlightSearchText(context, ssb, fullStr, searchStr);

        Pattern helpLinkPattern = Pattern.compile(LINK_PATTERN);
        Pattern emailPattern = Pattern.compile(EMAIL_PATTERN);

        Pattern pattern = Pattern.compile(HIGHLIGHT_PATTERN);
//        Matcher highlightMatcher = pattern.matcher(fullStr);

        while (true) {
            fullStr = ssb.toString();
//            LogU.d(TAG, "fullStr=" + fullStr);

            Matcher highlightMatcher = pattern.matcher(fullStr);
            if (highlightMatcher.find()) {
                String matchStr = highlightMatcher.group();
                LogU.d(TAG, "match=" + matchStr);

                int start = highlightMatcher.start(); //fullStr.indexOf(matchStr);//
                int end = highlightMatcher.end(); //start + matchStr.length();//

                LogU.d(TAG, "match start,end = " + start + "," + end);

                try {
                    ssb.replace(start, start + (HIGHLIGHT_START_TAG.length() - 1), "");
                    end -= (HIGHLIGHT_START_TAG.length() - 1);

                    ssb.replace(end - (HIGHLIGHT_END_TAG.length() - 1), end, "");
                    end -= (HIGHLIGHT_END_TAG.length() - 1);

                    StyleSpan boldStyleSpan = new StyleSpan(ResourcesCompat.getFont(context, R.font.spoqa_han_sans_bold).getStyle());
                    ssb.setSpan(boldStyleSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                  UnderlineSpan underlineSpan = new UnderlineSpan();

                    Matcher helpLinkMatcher = helpLinkPattern.matcher(matchStr);
                    Matcher emailMatcher = emailPattern.matcher(matchStr);

                    if (helpLinkMatcher.find()) {
                        String clickTag = helpLinkMatcher.group();
                        RxBusClickableSpan rxBusClickableSpan = new RxBusClickableSpan();
                        ssb.setSpan(rxBusClickableSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        ssb.replace(start, start + clickTag.length(), "");

                        clickTag = clickTag.substring(LINK_START_TAG.length() - 1, clickTag.length() - (LINK_END_TAG.length() - 1));
                        LogU.d(TAG, "clickTag=" + clickTag);
                        rxBusClickableSpan.setRxBusEvent(Constants.RX_BUS_EVENT_USE_INFO_LINK, clickTag);
                        textView.setMovementMethod(RxBusLinkMovementMethod.getInstance());

                    } else if (emailMatcher.find()) {
                        String email = emailMatcher.group();
                        LogU.d(TAG, "email=" + email);
                        RxBusClickableSpan rxBusClickableSpan = new RxBusClickableSpan();
                        rxBusClickableSpan.setRxBusEvent(Constants.RX_BUS_EVENT_MAIL_TO, email);
                        ssb.setSpan(rxBusClickableSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        textView.setMovementMethod(RxBusLinkMovementMethod.getInstance());

                    } else if (highRightColorId > 0) {
                        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(context.getColor(highRightColorId));
                        ssb.setSpan(foregroundColorSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    break;
                }
            } else {
                break;
            }
        }

        textView.setText(ssb);
        textView.requestLayout();
    }

    public static void highlightSearchText(Context context, TextView textView, String fullStr, String searchStr) {
        SpannableStringBuilder ssb = new SpannableStringBuilder();
        highlightSearchText(context, ssb, fullStr, searchStr);
        textView.setText(ssb);
        textView.requestLayout();
    }

    public static void highlightSearchText(Context context, SpannableStringBuilder ssb, String fullStr, String searchStr) {

//        textView.setText("");

        ssb.append(fullStr);

        if (searchStr == null || searchStr.isEmpty()) {
            return;
        }

        String[] splitSearchStr = searchStr.split("\\s");
//        Arrays.sort(splitSearchStr, new ComparatorStrLength());
//        List<String> searchStrList = Arrays.asList(splitSearchStr);
//        Collections.reverse(searchStrList);

        for (String sss : splitSearchStr) {

            if (sss.isEmpty()) {
                continue;
            }

            Pattern pattern = Pattern.compile("(?i)" + sss);
            Matcher matcher = pattern.matcher(fullStr);

//            int start = 0;
            while (matcher.find()) {
                String matchStr = matcher.group();

                LogU.e(TAG, "match=" + matchStr);

                int start = matcher.start(); //fullStr.indexOf(matchStr);//
                int end = matcher.end(); //start + matchStr.length();//

//                start = fullStr.indexOf(matchStr, start);
//                int end = start + matchStr.length();

                ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(context.getColor(R.color.colorBlueLight));
                StyleSpan boldStyleSpan = new StyleSpan(ResourcesCompat.getFont(context, R.font.spoqa_han_sans_bold).getStyle());

                try {
                    ssb.setSpan(foregroundColorSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    ssb.setSpan(boldStyleSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                } catch (Exception e) {
//                    e.printStackTrace();
                }
            }
        }
    }

    public static void showToast(Context context, int iconResId, int msgResId) {
        Toast toast = Toast.makeText(context, null, Toast.LENGTH_SHORT);
        View view = View.inflate(context, R.layout.view_toast_noti, null);
        ImageView toastIcon = view.findViewById(R.id.icon);
        toastIcon.setImageResource(iconResId);
        TextView toastText = view.findViewById(R.id.msg);
        toastText.setText(msgResId);
        toast.setView(view);
        toast.setGravity(Gravity.TOP, 0, (int) ScaleUtil.convertValue4dpi(310f));
        toast.show();
    }

//    public static void showDialog(Context context, AlertDialog dialog) {
//        dialog.setCancelable(false);
//        dialog.show();
//        TextView body = dialog.findViewById(android.R.id.message);
//        body.setPadding((int) ScaleUtil.convertValue4dpi(100f), (int) ScaleUtil.convertValue4dpi(10f), 0, 0);
//        body.setLineSpacing(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, ScaleUtil.convertValue4dpi(7.0f), context.getResources().getDisplayMetrics()), ScaleUtil.convertValue4dpi(1.0f));
//
//        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(ScaleUtil.convertValue4dpi(18f));
//        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(context.getColor(R.color.colorBlue));
//        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextSize(ScaleUtil.convertValue4dpi(18f));
//    }

    public static AlertDialog showDialog(Context context, AlertDialog.Builder builder) {

        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);

        dialog.show();

        TextView body = dialog.findViewById(android.R.id.message);
        body.setPadding(80, 30, 80, 0);
        body.setLineSpacing(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,7.0f, context.getResources().getDisplayMetrics()), 1.2f);

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(18f);
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(context.getColor(R.color.colorBlue));
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextSize(18f);

        return dialog;
    }

    public static AlertDialog.Builder getDialogBuilder(Context context, String title, int messageId, int iconId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(messageId);
        builder.setIcon(iconId);

        return builder;
    }

    public static void setVisibility(View view, boolean cause) {
//        LogU.d(TAG, "cause="+cause);
        view.setVisibility(cause ? View.VISIBLE : View.GONE);
    }

    public static void requestLayout(ViewGroup viewGroup) {
        try {
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View v = viewGroup.getChildAt(i);
                {
                    if (v instanceof ViewGroup) {
//                        v.requestLayout();
                        requestLayout((ViewGroup) v);

                    } else if (v instanceof TextView) {
//                    LogU.d(TAG, "requestLayout=" + ((TextView) v).getText().toString());
//                        v.postDelayed(v::requestLayout, 100);
                        v.requestLayout();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isKoreanLanguage() {
        String language = Locale.getDefault().getLanguage();

        switch (language) {
            case "ko":
                return true;
            default:
                return false;
        }
    }

//    public static void highlightTextView(Context context, ViewGroup viewGroup) {
//        for (int i = 0; i < viewGroup.getChildCount(); i++) {
//            View v = viewGroup.getChildAt(i);
//            {
//                if (v instanceof ViewGroup) {
//                    highlightTextView(context, (ViewGroup) v);
//
//                } else if (v instanceof TextView) {
////                    LogU.d(TAG, "requestLayout="+((TextView) v).getText().toString());
//                    ViewUtil.highlightTagText(context, (TextView) v, ((TextView) v).getText().toString());
//                    v.requestLayout();
//                }
//            }
//        }
//    }
}
