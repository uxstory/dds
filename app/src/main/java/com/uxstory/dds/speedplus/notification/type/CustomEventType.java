package com.uxstory.dds.speedplus.notification.type;

import com.uxstory.dds.speedplus.R;

public enum CustomEventType {
    NULL("null"                                             , false, false, false, false, null, -1),
    READY_WARM_UP("ready_warm_up"                           , true, false, true, true, null, R.string.noti_warm_up_ready),
    READY_PUMP("ready_pump_up"                              , true, false, true, true, null, R.string.noti_pump_ready),
    READY_CHANGE_BUR_AND_BLOCK("ready_change_bur_and_block" , true, false, true, true, null, R.string.noti_change_bur_and_block_ready),
    STOP_WARM_UP("stop_warm_up"                             , true, false, true, true, null, R.string.noti_warm_up_stop),
    STOP_PUMP("stop_pump_up"                                , true, false, true, true, null, R.string.noti_pump_stop),
    ;

    private String value;
    boolean backNoti;
    boolean popNoti;
    boolean toastNoti;
    boolean recordNoti;
    String intentAction;
    int msgResId;

    CustomEventType(String value, boolean backNoti, boolean popNoti, boolean toastNoti, boolean recordNoti, String intentAction, int msgResId) {
        this.value = value;
        this.backNoti = backNoti;
        this.popNoti = popNoti;
        this.toastNoti = toastNoti;
        this.recordNoti = recordNoti;
        this.intentAction = intentAction;
        this.msgResId = msgResId;
    }

    public String getString() {
        return value;
    }

    public boolean isBackNoti() {
        return backNoti;
    }

    public boolean isPopNoti() {
        return popNoti;
    }

    public boolean isToastNoti() {
        return toastNoti;
    }

    public boolean isRecordNoti() {
        return recordNoti;
    }

    public int getMsgResId() {
        return msgResId;
    }

    public int getIconResId() {
        if(isError()) {
            return R.drawable.icon_noti_error;

        } else {
            switch(this) {
                case READY_WARM_UP:
                case READY_PUMP:
                case READY_CHANGE_BUR_AND_BLOCK:
                case STOP_WARM_UP:
                case STOP_PUMP:
                    return R.drawable.icon_noti_work;
                default:
                    return R.drawable.icon_noti_00;
            }
        }
    }

    public int getToastIconResId() {
        if(isError()) {
            return R.drawable.icon_noti_error_white;

        } else {
            return R.drawable.icon_noti_work_white;
        }
    }

    public String getIntentAction() {
        return intentAction;
    }

    public static CustomEventType getType(String type) {
        for (CustomEventType eventType : CustomEventType.values()) {
            if (eventType.getString().equals(type)) {
                return eventType;
            }
        }
        return NULL;
    }

    public boolean isError() {
        return ErrorType.getType(getString()) != ErrorType.NULL;
    }
}
