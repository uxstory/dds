package com.uxstory.dds.speedplus.activity.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.model.DataCountModel;
import com.uxstory.dds.speedplus.model.type.BurType;
import com.uxstory.dds.speedplus.model.type.RestorationType;
import com.uxstory.dds.speedplus.util.LogU;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DataCountRecycleAdapter extends RecyclerView.Adapter<DataCountRecycleAdapter.ViewHolder> {

    private final static String TAG = DataCountRecycleAdapter.class.getSimpleName();

    private Context context;
    private List<DataCountModel> items;

    public DataCountRecycleAdapter(Context context, List<DataCountModel> items) {
        this.context = context;
        this.items = items;
    }

    public void setList(List<DataCountModel> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_data_count_small, parent, false);
        v.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LogU.d(TAG, "onBindViewHolder() position=" + position);

        DataCountModel item = items.get(position);
//        String index = String.valueOf(items.size() - position);

        RestorationType restorationType = item.getRestorationType();
        BurType burType = item.getBurType();
        if (restorationType != null) {
            holder.imageType.setImageResource(restorationType.getIconSmallResId());
            holder.txtName.setText(context.getString(restorationType.getNameResId()));
            holder.txtCount.setText(String.valueOf(item.getCount()));

        } else if (burType != null) {
            holder.imageType.setImageResource(burType.getIconSmallResId());
            holder.txtName.setText(item.getName());
            holder.txtCount.setText(String.valueOf(item.getCount()));

        } else {
            holder.imageType.setImageResource(R.drawable.icon_block_01);
            holder.txtName.setText(item.getName());
            holder.txtCount.setText(String.valueOf(item.getCount()));
        }

        ViewUtil.setVisibility(holder.viewLine, position < getItemCount() - 1);

        holder.txtName.requestLayout();
        holder.txtCount.requestLayout();
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.icon_type)
        ImageView imageType;

        @BindView(R.id.txt_name)
        TextView txtName;

        @BindView(R.id.txt_count)
        TextView txtCount;

        @BindView(R.id.view_line)
        View viewLine;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
