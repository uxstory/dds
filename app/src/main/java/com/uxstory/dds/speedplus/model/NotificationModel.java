package com.uxstory.dds.speedplus.model;

import com.uxstory.dds.speedplus.notification.type.NotificationKindType;

import java.util.List;

public class NotificationModel {
    private String name;
    private String serial;
    private String notificationTypeString;
    private NotificationKindType notificationKindType;
    private String regTime;

    private boolean isHeader;
    private boolean isSelected;
    private List<NotificationModel> childrenList;

    public List<NotificationModel> getChildrenList() {
        return childrenList;
    }

    public void setChildrenList(List<NotificationModel> childrenList) {
        this.childrenList = childrenList;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void setHeader(boolean header) {
        isHeader = header;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getNotificationTypeString() {
        return notificationTypeString;
    }

    public void setNotificationTypeString(String notificationTypeString) {
        this.notificationTypeString = notificationTypeString;
    }

    public NotificationKindType getNotificationKindType() {
        return notificationKindType;
    }

    public void setNotificationKindType(NotificationKindType notificationKindType) {
        this.notificationKindType = notificationKindType;
    }

    public String getRegTime() {
        return regTime;
    }

    public void setRegTime(String regTime) {
        this.regTime = regTime;
    }
}
