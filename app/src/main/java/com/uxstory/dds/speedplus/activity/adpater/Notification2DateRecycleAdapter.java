package com.uxstory.dds.speedplus.activity.adpater;

import android.content.Context;
import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.ui.OnScrollValueListener;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.database.DbQuery;
import com.uxstory.dds.speedplus.model.NotificationModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Notification2DateRecycleAdapter extends RecyclerView.Adapter<Notification2DateRecycleAdapter.ViewHolder> {

    private final static String TAG = Notification2DateRecycleAdapter.class.getSimpleName();

    private Context context;
    private List<String> items;

    private String title;

    private String serial;

    private String search;
    private Set<String> searchNotiTypeList;

    private final int topBarTop = 260;
//    private final int topBarTopRemove = 260 / 2;
//    private final int topBarBottomRemove = 260 * 2;

    private OnScrollValueListener onScrollValueListener;


    public void setSerial(String serial) {
        this.serial = serial;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public void setSearchNotiTypeList(Set<String> searchNotiTypeList) {
        this.searchNotiTypeList = searchNotiTypeList;
    }

    public Notification2DateRecycleAdapter(Context context, List<String> items, OnScrollValueListener onScrollValueListener) {
        this.context = context;
        this.items = items;
        this.onScrollValueListener = onScrollValueListener;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setList(List<String> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_noti_device_day, parent, false);
        v.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        LogU.d(TAG, "onBindViewHolder() position=" + position);

        String date = items.get(position);
        holder.txtNotiDate.setText(date);
        holder.txtNotiDate.requestLayout();

        if (position == 0) {
            setNotiMessageList(holder.recyclerAdapter, date);
        }

        holder.dateBar.setSelected(position == 0);
        setBodyVisible(holder, position);

        holder.dateBar.setOnClickListener(v -> {
            v.setSelected(!v.isSelected());
            if (v.isSelected()) {
                setNotiMessageList(holder.recyclerAdapter, date);
            }
            setBodyVisible(holder, position);
        });

        holder.itemView.getViewTreeObserver().removeOnScrollChangedListener(holder.onScrollChangedListener);

        NotificationModel notificationModel = new NotificationModel();
        notificationModel.setName(title);
        notificationModel.setRegTime(date);

        holder.onScrollChangedListener = () -> {
            Rect outRect = new Rect();
            holder.itemView.getGlobalVisibleRect(outRect);

//            LogU.d(TAG, "title="+title+","+item.getSerial() + ",position=" + position + ",top=" + outRect.top+ ",bottom=" + outRect.bottom +",date="+item.getRegTime());

            if (outRect.top < topBarTop && outRect.top + outRect.height() > topBarTop) {
//                RxBus.get().send(Constants.RX_BUS_EVENT_NOTI_TOP_BAR_INFO, notificationModel);
                if (onScrollValueListener != null) {
                    onScrollValueListener.onScrollValue(notificationModel.getName(), notificationModel.getRegTime());
                }
            }
//            else {
//                outRect = new Rect();
//                holder.dateBar.getGlobalVisibleRect(outRect);
//                if (outRect.top <= topBarTopRemove && outRect.bottom <= topBarBottomRemove) {
//                    RxBus.get().send("set_top_bar_sub", "");
//                }
//            }
        };

        holder.itemView.getViewTreeObserver().addOnScrollChangedListener(holder.onScrollChangedListener);

    }

    private void setBodyVisible(ViewHolder holder, int position) {
        ViewUtil.setVisibility(holder.divLine, !holder.dateBar.isSelected() && position < getItemCount() - 1);
        ViewUtil.setVisibility(holder.recyclerView, holder.dateBar.isSelected());
    }

    private void setNotiMessageList(Notification3MessageRecycleAdapter recyclerAdapter, String date) {
        if (recyclerAdapter.getItemCount() == 0 || search != recyclerAdapter.getSearch() || (search != null && !search.equals(recyclerAdapter.getSearch()))) {
            List<NotificationModel> list = DbQuery.selectNotiMessageList(serial, date, searchNotiTypeList);
            recyclerAdapter.setList(list);
            recyclerAdapter.setSearch(search);
            recyclerAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_noti_date)
        TextView txtNotiDate;

        @BindView(R.id.recycler_noti_device_day_msg)
        RecyclerView recyclerView;

        @BindView(R.id.layout_date_bar)
        View dateBar;

        @BindView(R.id.div_line)
        View divLine;

        ViewTreeObserver.OnScrollChangedListener onScrollChangedListener;
        Notification3MessageRecycleAdapter recyclerAdapter;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            LinearLayoutManager layoutManager = new LinearLayoutManager(context) {
                @Override
                public boolean canScrollVertically() {
                    return false;
                }
            };
            recyclerView.setLayoutManager(layoutManager);

            recyclerAdapter = new Notification3MessageRecycleAdapter(context, new ArrayList<>());
            recyclerView.setAdapter(recyclerAdapter);

        }
    }
}
