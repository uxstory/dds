package com.uxstory.dds.speedplus.notification.type;

public enum NotificationKindType {
    NULL,
    EVENT,
    CUSTOM_EVENT,
    APP,
    ;

    public static NotificationKindType getType(String type) {
        for (NotificationKindType eventType : NotificationKindType.values()) {
            if (eventType.toString().equals(type)) {
                return eventType;
            }
        }
        return NULL;
    }
}
