package com.uxstory.dds.speedplus.util;

import com.uxstory.dds.speedplus.application.GlobalApplication;

public class ScaleUtil {

    /**
     * ScaleLayout 적용시 사용.
     * @param value
     * @return
     */
    public static float convertValue4Scale(float value) {
        return value / GlobalApplication.getDensity();
    }

    /**
     * ScaleLayout 이외에서 사용.
     * @param value
     * @return
     */
    public static float convertValue4dpi(float value) {
        return value * GlobalApplication.getDpiMultiplier();
    }

    public static float converScaleToPixel(float value) {
        return value * GlobalApplication.getDensity();
    }
}
