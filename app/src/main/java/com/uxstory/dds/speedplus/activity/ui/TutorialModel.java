package com.uxstory.dds.speedplus.activity.ui;

import android.widget.ImageView;

public class TutorialModel {
    private int imgId;
    private int txtTitleId;
    private int txtMsgId;
    private ImageView dot;

    public TutorialModel(int imgId, int txtTitleId, int txtMsgId, ImageView dot) {
        this.imgId = imgId;
        this.txtTitleId = txtTitleId;
        this.txtMsgId = txtMsgId;
        this.dot = dot;
    }

    public int getImgId() {
        return imgId;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }

    public int getTxtTitleId() {
        return txtTitleId;
    }

    public void setTxtTitleId(int txtTitleId) {
        this.txtTitleId = txtTitleId;
    }

    public int getTxtMsgId() {
        return txtMsgId;
    }

    public void setTxtMsgId(int txtMsgId) {
        this.txtMsgId = txtMsgId;
    }

    public ImageView getDot() {
        return dot;
    }

    public void setDot(ImageView dot) {
        this.dot = dot;
    }
}
