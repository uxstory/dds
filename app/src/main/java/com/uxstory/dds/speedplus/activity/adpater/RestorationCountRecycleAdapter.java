package com.uxstory.dds.speedplus.activity.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.model.DataCountModel;
import com.uxstory.dds.speedplus.model.type.RestorationType;
import com.uxstory.dds.speedplus.util.LogU;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RestorationCountRecycleAdapter extends RecyclerView.Adapter<RestorationCountRecycleAdapter.ViewHolder> {

    private final static String TAG = RestorationCountRecycleAdapter.class.getSimpleName();

    private Context context;
    private List<DataCountModel> items;

    public RestorationCountRecycleAdapter(Context context, List<DataCountModel> items) {
        this.context = context;
        this.items = items;
    }

    public void setList(List<DataCountModel> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_data_count, parent, false);
        v.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LogU.d(TAG, "onBindViewHolder() position=" + position);

        DataCountModel item = items.get(position);
//        String index = String.valueOf(items.size() - position);

        RestorationType restorationType = item.getRestorationType();
        holder.imageType.setImageResource(restorationType.getIconSmallResId());
        holder.txtName.setText(context.getString(restorationType.getNameResId()));
        holder.txtCount.setText(String.valueOf(item.getCount()));
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.icon_type)
        ImageView imageType;

        @BindView(R.id.txt_name)
        TextView txtName;

        @BindView(R.id.txt_count)
        TextView txtCount;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
