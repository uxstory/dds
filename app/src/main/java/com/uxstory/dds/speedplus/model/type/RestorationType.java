package com.uxstory.dds.speedplus.model.type;

import android.content.Context;

import com.uxstory.dds.speedplus.R;

public enum RestorationType {
    NULL(R.string.txt_restoration_null, R.drawable.icon_type_01_c, R.drawable.icon_type_02_c, R.drawable.icon_type_03_c, R.color.colorChartPie8),
    CR(R.string.txt_restoration_cr, R.drawable.icon_type_01_c, R.drawable.icon_type_02_c, R.drawable.icon_type_03_c, R.color.colorChartPie1),
    PC(R.string.txt_restoration_pc, R.drawable.icon_type_01_e, R.drawable.icon_type_02_e, R.drawable.icon_type_03_e, R.color.colorChartPie2),
    CO(R.string.txt_restoration_co, R.drawable.icon_type_01_b, R.drawable.icon_type_02_b, R.drawable.icon_type_03_b, R.color.colorChartPie3),
    IN(R.string.txt_restoration_in, R.drawable.icon_type_01_d, R.drawable.icon_type_02_d, R.drawable.icon_type_03_d, R.color.colorChartPie4),
    LA(R.string.txt_restoration_la, R.drawable.icon_type_01_f, R.drawable.icon_type_02_f, R.drawable.icon_type_03_f, R.color.colorChartPie5),
    BR(R.string.txt_restoration_br, R.drawable.icon_type_01_a, R.drawable.icon_type_02_a, R.drawable.icon_type_03_a, R.color.colorChartPie6),
    UN(R.string.txt_restoration_un, R.drawable.icon_type_01_c, R.drawable.icon_type_02_c, R.drawable.icon_type_03_c, R.color.colorChartPie7),
    ;

    private int stringResId;
    private int iconSmallResId;
    private int iconNormalResId;
    private int iconLargeResId;
    private int colorResId;

    RestorationType(int stringResId, int iconSmallResId, int iconNormalResId, int iconLargeResId, int colorResId) {
        this.stringResId = stringResId;
        this.iconSmallResId = iconSmallResId;
        this.iconNormalResId = iconNormalResId;
        this.iconLargeResId = iconLargeResId;
        this.colorResId = colorResId;
    }

    public int getNameResId() {
        return this.stringResId;
    }

    public int getIconSmallResId() {
        return this.iconSmallResId;
    }

    public int getIconNormalResId() {
        return this.iconNormalResId;
    }

    public int getIconLargeResId() {
        return this.iconLargeResId;
    }

    public int getColorResId() {
        return colorResId;
    }

    public static RestorationType getType(String name) {
        if(name == null) {
            return NULL;
        }
        for (RestorationType type : RestorationType.values()) {
            if (name.toUpperCase().indexOf(type.toString()) == 0) {
                return type;
            }
        }
        return UN;
    }

    public static String getRestorationString(Context context, String restoration) {
        RestorationType type = RestorationType.getType(restoration);
        switch (type) {
            case NULL:
            case UN:
                return context.getString(type.getNameResId());
            default:
                return context.getString(type.getNameResId()) +" "+ restoration.toUpperCase().replaceAll(type.name(), "");
        }
    }

}
