package com.uxstory.dds.speedplus.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.viewpager2.widget.ViewPager2;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.adpater.TutorialRecycleAdapter;
import com.uxstory.dds.speedplus.activity.ui.OnSingleClickListener;
import com.uxstory.dds.speedplus.activity.ui.TutorialModel;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.config.Preference;
import com.uxstory.dds.speedplus.util.LogU;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TutorialActivity extends AbstractBaseActivity {

    private final static String TAG = TutorialActivity.class.getSimpleName();

    @BindView(R.id.pager)
    ViewPager2 pager;

    @BindView(R.id.img_navi_dot_1)
    ImageView imgNaviDot1;

    @BindView(R.id.img_navi_dot_2)
    ImageView imgNaviDot2;

    @BindView(R.id.img_navi_dot_3)
    ImageView imgNaviDot3;

    @BindView(R.id.btn_goto_dash)
    Button btnGotoDash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        ButterKnife.bind(this);

        Preference.setPreferenceValue(TutorialActivity.this, Preference.PreferenceType.ENTERED_TUTORIAL, true);

        List<TutorialModel> list = new ArrayList<>();
        TutorialModel model = new TutorialModel(R.drawable.img_on_01, R.string.txt_tutorial_title_1, R.string.txt_tutorial_msg_1, imgNaviDot1);
        list.add(model);
        model = new TutorialModel(R.drawable.img_on_02, R.string.txt_tutorial_title_2, R.string.txt_tutorial_msg_2, imgNaviDot2);
        list.add(model);
        model = new TutorialModel(R.drawable.img_on_03, R.string.txt_tutorial_title_3, R.string.txt_tutorial_msg_3, imgNaviDot3);
        list.add(model);

        TutorialRecycleAdapter adapter = new TutorialRecycleAdapter(this, list);
        pager.setAdapter(adapter);

        pager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                LogU.d(TAG, "onPageSelected()=" + position);

                ViewUtil.setVisibility(btnGotoDash, position == list.size() -1);

                ImageView imgDot = list.get(position).getDot();
                for(TutorialModel model : list) {
                    model.getDot().setSelected(model.getDot() == imgDot);
                }
            }
        });

        btnGotoDash.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivity(new Intent(TutorialActivity.this, GuideActivity.class));
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
