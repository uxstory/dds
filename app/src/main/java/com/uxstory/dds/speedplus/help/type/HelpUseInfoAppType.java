package com.uxstory.dds.speedplus.help.type;

import android.content.Context;

import com.uxstory.dds.speedplus.R;

public enum HelpUseInfoAppType {

    INFO(R.drawable.icon_info_app, R.string.txt_use_info_app_category_info, R.array.help_app_info_array),
    CONNECT(R.drawable.icon_info_link, R.string.txt_use_info_app_category_connect, R.array.help_app_connect_array),
    MILLING(R.drawable.icon_info_work, R.string.txt_use_info_app_category_milling, R.array.help_app_milling_array),
    BUT_WATER(R.drawable.icon_info_product, R.string.txt_use_info_app_category_bur_water, R.array.help_app_bur_water_array),
    NOTI(R.drawable.icon_info_noti, R.string.txt_use_info_app_category_noti, R.array.help_app_noti_array),
    PROBLEM(R.drawable.icon_info_problem, R.string.txt_use_info_app_category_problem, R.array.help_app_problem_array),
    ;

    private int icon;
    private int string;
    private int contentsArray;

    HelpUseInfoAppType(int icon, int string, int contentsArray) {
        this.icon = icon;
        this.string = string;
        this.contentsArray = contentsArray;
    }

    public int getIcon() {
        return icon;
    }

    public int getString() {
        return string;
    }

    public String[] getContentsArray(Context context) {
        return context.getResources().getStringArray(contentsArray);
    }
}
