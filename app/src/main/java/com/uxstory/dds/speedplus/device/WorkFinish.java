package com.uxstory.dds.speedplus.device;

public class WorkFinish {
    String elapsedTime;

    public String getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(String elapsedTime) {
        this.elapsedTime = elapsedTime;
    }
}
