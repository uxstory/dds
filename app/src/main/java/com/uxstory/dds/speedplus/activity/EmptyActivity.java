package com.uxstory.dds.speedplus.activity;

import android.os.Bundle;
import android.util.Log;

import com.uxstory.dds.speedplus.util.LogU;

public class EmptyActivity extends AbstractBaseActivity {

    private final static String TAG = EmptyActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
    }
}
