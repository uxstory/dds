package com.uxstory.dds.speedplus.model.type;

import com.uxstory.dds.speedplus.R;

public enum BurType {
    LEFT(R.drawable.icon_bur_01),
    RIGHT(R.drawable.icon_bur_02),
    ;

    private int iconSmallResId;

    BurType(int iconSmallResId) {
        this.iconSmallResId = iconSmallResId;
    }

    public int getIconSmallResId() {
        return iconSmallResId;
    }
}
