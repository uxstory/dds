package com.uxstory.dds.speedplus.activity.adpater;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.uxstory.dds.speedplus.activity.fragment.GuideFragment;

public class GuideViewPagerFragmentStateAdapter extends FragmentStateAdapter {

    private static final int ITEM_SIZE = 3;

    public GuideViewPagerFragmentStateAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return GuideFragment.newInstance(position);
    }

    @Override
    public int getItemCount() {
        return ITEM_SIZE;
    }


}
