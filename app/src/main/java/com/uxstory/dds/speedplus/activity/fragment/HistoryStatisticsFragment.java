package com.uxstory.dds.speedplus.activity.fragment;

import android.content.Context;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.widget.ViewPager2;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.adpater.ViewPagerFragmentStateAdapter;
import com.uxstory.dds.speedplus.util.LogU;

import java.util.ArrayList;

import butterknife.BindView;

public class HistoryStatisticsFragment extends AbstractBaseFragment {

    private final static String TAG = HistoryStatisticsFragment.class.getSimpleName();

    @BindView(R.id.pager)
    ViewPager2 pager;

    @BindView(R.id.btn_mode_day)
    Button btnModeDay;

    @BindView(R.id.btn_mode_month)
    Button btnModeMonth;

    @BindView(R.id.btn_calendar)
    ImageButton btnOpenCalendar;

    private FragmentManager fragmentManager;
    private ArrayList<Fragment> fragments = new ArrayList<>();

    private HistoryStatisticsPageFragment historyStatisticsPageFragmentDays = new HistoryStatisticsPageFragment();
    private HistoryStatisticsPageFragment historyStatisticsPageFragmentMonths = new HistoryStatisticsPageFragment();

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        setLayoutId(R.layout.fragment_history_statistics);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        fragmentManager = getChildFragmentManager();

        historyStatisticsPageFragmentDays.setDevice(getDevice());
        historyStatisticsPageFragmentDays.setMode(0);
        historyStatisticsPageFragmentMonths.setDevice(getDevice());
        historyStatisticsPageFragmentMonths.setMode(1);

        fragments.add(historyStatisticsPageFragmentDays);
        fragments.add(historyStatisticsPageFragmentMonths);

        btnModeDay.setEnabled(false);
        btnModeMonth.setEnabled(true);

        btnModeDay.setOnClickListener(v -> {
            btnModeMonth.setEnabled(true);
            btnModeDay.setEnabled(false);
            btnOpenCalendar.setEnabled(true);

            historyStatisticsPageFragmentDays.closeCalendar();
            pager.setCurrentItem(0);
        });

        btnModeMonth.setOnClickListener(v -> {
            btnModeDay.setEnabled(true);
            btnModeMonth.setEnabled(false);
            btnOpenCalendar.setEnabled(false);

            historyStatisticsPageFragmentDays.closeCalendar();
            pager.setCurrentItem(1);
        });

        btnOpenCalendar.setOnClickListener(v -> historyStatisticsPageFragmentDays.openCalendar());

    }

    public void updateView() {
        if(pager.getAdapter() == null) {
            setPager();
        }
    }

    private void setPager() {

        ViewPagerFragmentStateAdapter viewPagerFragmentStateAdapter = new ViewPagerFragmentStateAdapter(this);
        viewPagerFragmentStateAdapter.setList(fragments);

        pager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                switch (position) {
                    case 0:
                        historyStatisticsPageFragmentDays.updateView();
                        break;
                    case 1:
                        historyStatisticsPageFragmentMonths.updateView();
                        break;
                }
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }
        });

        pager.setAdapter(viewPagerFragmentStateAdapter);
        pager.setUserInputEnabled(false);
//        pager.setPageTransformer(new ZoomOutPageTransformer());
        pager.setOffscreenPageLimit(1);
        pager.setCurrentItem(0);
    }

    private void removeChildFragments() {
        fragmentManager.beginTransaction().remove(historyStatisticsPageFragmentDays).remove(historyStatisticsPageFragmentMonths).commitAllowingStateLoss();
    }

    @Override
    public void onDestroyView() {
        LogU.d(TAG, "onDestroyView() activity=" + getActivity() + ", this=" + this);
        removeChildFragments();
        super.onDestroyView();
    }
}
