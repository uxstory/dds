package com.uxstory.dds.speedplus.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.viewpager2.widget.ViewPager2;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.adpater.GuideViewPagerFragmentStateAdapter;
import com.uxstory.dds.speedplus.activity.ui.OnSingleClickListener;
import com.uxstory.dds.speedplus.activity.ui.ZoomOutPageTransformer;
import com.uxstory.dds.speedplus.config.Constants;
import com.uxstory.dds.speedplus.config.Preference;

import butterknife.BindView;
import butterknife.ButterKnife;
import pe.warrenth.rxbus2.RxBus;
import pe.warrenth.rxbus2.Subscribe;

public class GuideActivity extends AbstractBaseActivity {

    private final static String TAG = GuideActivity.class.getSimpleName();

    @BindView(R.id.pager)
    ViewPager2 pager;

    @BindView(R.id.img_close)
    ImageView imgClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);
        ButterKnife.bind(this);

        Preference.setPreferenceValue(GuideActivity.this, Preference.PreferenceType.ENTERED_GUIDE, true);

        imgClose.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                close();
            }
        });

        setPager();
    }

    @Override
    protected void onStart() {
        super.onStart();
        RxBus.get().register(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        RxBus.get().unResister(this);
    }

    @Subscribe(eventTag = Constants.RX_BUS_EVENT_NEXT_PAGE)
    public void nextPage() {
        int i = pager.getCurrentItem() + 1;
        i = (i >= pager.getAdapter().getItemCount()) ? pager.getAdapter().getItemCount() - 1 : i;
        pager.setCurrentItem(i, true);
    }

    @Subscribe(eventTag = Constants.RX_BUS_EVENT_CLOSE)
    public void close() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    private void setPager() {

        GuideViewPagerFragmentStateAdapter guideViewPagerFragmentStateAdapter = new GuideViewPagerFragmentStateAdapter(this);
        pager.setAdapter(guideViewPagerFragmentStateAdapter);
//        pager.setUserInputEnabled(false);

        pager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }
        });

        pager.setPageTransformer(new ZoomOutPageTransformer());
        pager.setOffscreenPageLimit(2);
        pager.setCurrentItem(0);
    }


}
