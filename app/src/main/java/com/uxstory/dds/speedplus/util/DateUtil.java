package com.uxstory.dds.speedplus.util;

import android.content.Context;

import com.uxstory.dds.speedplus.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public final class DateUtil {
    private final static String TAG = DateUtil.class.getSimpleName();

    public enum TIME_UNIT {MILLIS, SECS, MINS, HOURS, DAYS, MONTH, YEAR};

    private static String INVALID_DATE_ERR = "Please provide a valid Date.";
    private static String INVALID_CAL_ERR = "Please provide a valid Calendar.";
    private static String STRING_FMT_ERR = "Please provide a valid String.";

    private static final long MILLIS_IN_SEC = 1000;
    private static final long MILLIS_IN_MIN = MILLIS_IN_SEC * 60;
    private static final long MILLIS_IN_HOUR = MILLIS_IN_MIN * 60;
    private static final long MILLIS_IN_DAY = MILLIS_IN_HOUR * 24;


    private static final int SEC = 60;
    private static final int MIN = 60;
    private static final int HOUR = 24;
    private static final int DAY = 30;
    private static final int MONTH = 12;

    public static String convertFormatDate(String toFormat, String dateTime) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = format.parse(dateTime);

            format = new SimpleDateFormat(toFormat);
            return format.format(date);
        } catch (ParseException e) {
            return "";
        }
    }

    public static String calculateTime(Context context, String time) {

        long regTime = StringUtil.convertDateToTimeMillis(time);
        long curTime = System.currentTimeMillis();
        long diffTime = (curTime - regTime) / 1000;

        String msg = null;

        if (diffTime < SEC) {
            // sec
            msg = diffTime + context.getString(R.string.txt_pre_time_unit_sec);
        } else if ((diffTime /= SEC) < MIN) {
            // min
            msg = diffTime + context.getString(R.string.txt_pre_time_unit_min);
        } else if ((diffTime /= MIN) < HOUR) {
            // hour
            msg = (diffTime) + context.getString(R.string.txt_pre_time_unit_hour);
        } else if ((diffTime /= HOUR) < DAY) {
            // day
            msg = (diffTime) + context.getString(R.string.txt_pre_time_unit_day);
        } else if ((diffTime /= DAY) < MONTH) {
            // day
            msg = (diffTime) + context.getString(R.string.txt_pre_time_unit_month);
        } else {
            msg = (diffTime) + context.getString(R.string.txt_pre_time_unit_year);
        }

        return msg;
    }


    public static String getDateStringAddDate(int mode, int amount, Calendar calendar) {
        Calendar calendar1 = (Calendar) calendar.clone();
        calendar1.add(mode, amount);
        LogU.d(TAG, "getDateStringAddDate=" + calendar1.getTime());
        return DateUtil.formatToShortDate(calendar1);
    }

    public static Calendar getCalendar(String date) {
        Calendar calendar = null;
        if (date != null) {
            date = StringUtil.convertDateToRawFormat(date);
            String[] splitDate = date.split("\\-");
            calendar = Calendar.getInstance();
            calendar.set(Integer.valueOf(splitDate[0]), Integer.valueOf(splitDate[1]) - 1, Integer.valueOf(splitDate[2]));
        }
        return calendar;
    }


    /**
     * Formats a Date object, given a format string consistent with
     * <code>SimpleDateFormat</code> class.
     *
     * @param date   - A Date object to be formatted.
     * @param format - The desired format (for <code>SimpleDateFormat</code> class.
     * @return A date formatted as a string.
     */
    public static String formatDate(Date date, String format) {
        if (date == null) {
            throw new NullPointerException(INVALID_DATE_ERR);
        }
        if (format == null) {
            throw new NullPointerException(STRING_FMT_ERR);
        }
        if (format.length() == 0) {
            throw new IllegalArgumentException(STRING_FMT_ERR);
        }

        SimpleDateFormat dateFormatter = new SimpleDateFormat(format, Locale.getDefault());
        return dateFormatter.format(date);
    }

    /**
     * Creates a date from a formatted string consistent with the <code>
     * SimpleDateFormat</code> class.
     *
     * @param formattedString - A formatted date as a string consistent with the
     *                        <code>SimpleDateFormat</code> class.
     * @return A Date object created from the date in the formattedString.
     * @throws ParseException
     */
    public static Date getDateFromString(String formattedString) throws ParseException {
        if (formattedString == null) {
            throw new NullPointerException(STRING_FMT_ERR);
        }
        if (formattedString.length() == 0) {
            throw new IllegalArgumentException(STRING_FMT_ERR);
        }

        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        return dateFormatter.parse(formattedString);

    }

    /**
     * Formats a Calendar object, given a format string consistent with the <code>
     * SimpleDateFormat</code> class..
     *
     * @param calendar - A Calendar object to be formatted.
     * @param format   - The desired format consistent with the <code>
     *                 SimpleDateFormat</code> class.
     * @return A date formatted as a string.
     */
    public static String formatDate(Calendar calendar, String format) {
        if (calendar == null) {
            throw new NullPointerException(INVALID_CAL_ERR);
        }
        if (format == null) {
            throw new NullPointerException(STRING_FMT_ERR);
        }
        if (format.length() == 0) {
            throw new IllegalArgumentException(STRING_FMT_ERR);
        }

        SimpleDateFormat dateFormatter = new SimpleDateFormat(format, Locale.getDefault());

        return dateFormatter.format(calendar.getTime());
    }

    /**
     * Creates a date from a formatted string consistent with the <code>
     * SimpleDateFormat</code> class.
     *
     * @param formattedString - A formatted date as a string consistent with the
     *                        <code>impleDateFormat</code> class.
     * @return A Calendar object created from the date in the formattedString.
     * @throws ParseException
     */
    public static Calendar getCalendarFromString(String formattedString) throws ParseException {
        if (formattedString == null) {
            throw new NullPointerException(STRING_FMT_ERR);
        }
        if (formattedString.length() == 0) {
            throw new IllegalArgumentException(STRING_FMT_ERR);
        }

        SimpleDateFormat dateFormatter = new SimpleDateFormat(formattedString, Locale.getDefault());
        Date date = dateFormatter.parse(formattedString);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        return cal;
    }

    /**
     * Formats the specified calendar into a string with short date format (yyyy-MM-dd).
     * For example, 01/01/2013.
     *
     * @param cal - The calendar to format.
     * @return A String formatted into a short date (yyyy-MM-dd).
     */
    public static String formatToShortDate(Calendar cal) {
        if (cal == null) {
            throw new NullPointerException(INVALID_CAL_ERR);
        }
        return formatDate(cal, "yyyy-MM-dd");
    }

    /**
     * Formats the specified date into a string with short date format (yyyy-MM-dd).
     * For example, 01/01/2013.
     *
     * @param date - The date to format.
     * @return A String formatted into a short date (yyyy-MM-dd).
     */
    public static String formatToShortDate(Date date) {
        if (date == null) {
            throw new NullPointerException(INVALID_DATE_ERR);
        }
        return formatDate(date, "yyyy-MM-dd");
    }

    /**
     * Formats the specified calendar into a string with short time format
     * (HH:mm aa). For example, 09:00 AM.
     *
     * @param cal - The calendar to format.
     * @return A String formatted into a short time.
     */
    public static String formatToShortTime(Calendar cal) {
        if (cal == null) {
            throw new NullPointerException(INVALID_CAL_ERR);
        }
        return formatDate(cal, "hh:mm aa");
    }

    /**
     * Formats the specified date into a string with short time format
     * (HH:mm aa). For example, 09:00 AM.
     *
     * @param date - The date to format.
     * @return A String formatted into a short time.
     */
    public static String formatToShortTime(Date date) {
        if (date == null) {
            throw new NullPointerException(INVALID_DATE_ERR);
        }
        return formatDate(date, "hh:mm aa");
    }


    /**
     * Returns the time difference between two calendars in the specified time
     * unit. If the first calendar's date occurs after the second calendar's,
     * returns a negative value.
     *
     * @param cal1     - The first calendar.
     * @param cal2     - The second calendar.
     * @param timeUnit - The <code>TIME_UNIT</code> to return the difference in.
     * @return Time difference between the calendars. If cal1 is after cal2,
     * returns a negative value.
     */
    public static long getDiff(Calendar cal1, Calendar cal2,
                               TIME_UNIT timeUnit) {

        if (cal1 == null || cal2 == null || timeUnit == null) {
            throw new NullPointerException(INVALID_DATE_ERR);
        }

        long timeDiffInMillis = cal2.getTimeInMillis() - cal1.getTimeInMillis();

        switch (timeUnit) {
            case SECS:
                return timeDiffInMillis / MILLIS_IN_SEC;
            case MINS:
                return timeDiffInMillis / MILLIS_IN_MIN;
            case HOURS:
                return timeDiffInMillis / MILLIS_IN_HOUR;
            case DAYS:
                return timeDiffInMillis / MILLIS_IN_DAY;
            case MILLIS:
            default:
                return timeDiffInMillis;
        }

    }

    /**
     * Returns the time difference between two dates in the specified time
     * unit. If the first date occurs after the second date, returns a negative.
     *
     * @param date1    - The first date.
     * @param date2    - The second date.
     * @param timeUnit - The <code>TIME_UNIT</code> to return the difference in.
     * @return Time difference between the calendars. If date1 is after date2,
     * returns a negative value.
     */
    public static long getDiff(Date date1, Date date2, TIME_UNIT timeUnit) {
        if (date1 == null || date2 == null || timeUnit == null) {
            throw new NullPointerException();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        return getDiff(cal1, cal2, timeUnit);

    }

    /**
     * Given a <code>Calendar</code>, adds the given amount of time to the
     * calendar. If the given amount of time is negative, it subtracts the time,
     * producing a date earlier than the given date.
     *
     * @param cal      - The Calendar to add time to.
     * @param amount   - The amount of time to add, in the time unit specified in
     *                 <code>timeUnit</code>.
     * @param timeUnit - The unit of time to add.
     * @return A Calendar with the new date/time.
     */
    public static Calendar addTime(Calendar cal, long amount, TIME_UNIT timeUnit) {
        if (cal == null) {
            throw new NullPointerException(INVALID_CAL_ERR);
        }
        switch (timeUnit) {
            case MILLIS:
                cal.setTimeInMillis(cal.getTimeInMillis() + amount);
                return cal;
            case SECS:
                long secondsToAdd = cal.getTimeInMillis() + amount * MILLIS_IN_SEC;
                cal.setTimeInMillis(secondsToAdd);
                return cal;
            case MINS:
                long minsToAdd = cal.getTimeInMillis() + amount * MILLIS_IN_MIN;
                cal.setTimeInMillis(minsToAdd);
                return cal;
            case HOURS:
                long hoursToAdd = cal.getTimeInMillis() + amount * MILLIS_IN_HOUR;
                cal.setTimeInMillis(hoursToAdd);
                return cal;
            case DAYS:
                cal.add(Calendar.DATE, (int) amount);
                return cal;
            case MONTH:
                cal.add(Calendar.MONTH, (int) amount - 1);
                return cal;
            case YEAR:
                cal.add(Calendar.YEAR, (int) amount);
                return cal;
            default:
                return null;
        }
    }

    /**
     * Given a <code>Date</code>, adds the given amount of time to the
     * date. If the given amount of time is negative, it subtracts the time,
     * producing a date earlier than the given date.
     *
     * @param date     - The <code>Date</code> to add time to.
     * @param amount   - The amount of time to add, in the time unit specified in
     *                 <code>timeUnit</code>.
     * @param timeUnit - The unit of time to add.
     * @return A <code>Date</code> with the new date/time.
     */
    public static Date addTime(Date date, long amount, TIME_UNIT timeUnit) {
        if (date == null) {
            throw new NullPointerException(INVALID_DATE_ERR);
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        addTime(cal, amount, timeUnit);
        return cal.getTime();
    }

}
