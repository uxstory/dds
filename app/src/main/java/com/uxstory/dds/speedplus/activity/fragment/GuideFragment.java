package com.uxstory.dds.speedplus.activity.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.ui.OnSingleClickListener;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.config.Constants;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import pe.warrenth.rxbus2.RxBus;

public class GuideFragment extends Fragment {

    @BindView(R.id.layout_guide_overlay_black_1)
    View overlayBlack1;
    @BindView(R.id.layout_guide_overlay_black_2)
    View overlayBlack2;
    @BindView(R.id.layout_guide_overlay_black_3)
    View overlayBlack3;


    @BindView(R.id.layout_guide_1)
    View layoutGuide1;
    @BindView(R.id.layout_guide_1_1)
    View layoutGuide11;
    @BindView(R.id.layout_guide_1_2)
    View layoutGuide12;

    @BindView(R.id.layout_guide_2)
    View layoutGuide2;
    @BindView(R.id.layout_guide_2_1)
    View layoutGuide21;
    @BindView(R.id.layout_guide_2_2)
    View layoutGuide22;
    @BindView(R.id.layout_guide_2_3)
    View layoutGuide23;
    @BindView(R.id.layout_guide_2_4)
    View layoutGuide24;

    @BindView(R.id.layout_guide_3)
    View layoutGuide3;
    @BindView(R.id.layout_guide_3_1)
    View layoutGuide31;
    @BindView(R.id.layout_guide_3_2)
    View layoutGuide32;
    @BindView(R.id.layout_guide_3_3)
    View layoutGuide33;
    @BindView(R.id.layout_guide_3_4)
    View layoutGuide34;

    @BindView(R.id.txt_guide_3_3_1)
    TextView txtGuide331;
    @BindView(R.id.txt_guide_3_3_2)
    TextView txtGuide332;

    @BindView(R.id.btn_next_1)
    View btnNext1;

    @BindView(R.id.btn_next_2)
    View btnNext2;

    @BindView(R.id.btn_start)
    View btnStart;

    private List<View> viewList = new ArrayList<>();

    private Animation aniSlowlyAppear;
    private Animation aniSlowlyDisappear;

    private int position;


    public static GuideFragment newInstance(int position) {
        GuideFragment fragment = new GuideFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments() != null ? getArguments().getInt("position") : 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        int layoutId;
        if(ViewUtil.isKoreanLanguage()) {
            layoutId = R.layout.fragment_guide;
        } else {
            layoutId = R.layout.fragment_guide_en;
        }

        View view = inflater.inflate(layoutId, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        aniSlowlyDisappear = AnimationUtils.loadAnimation(getContext(), R.anim.slowly_fadeout);
        aniSlowlyAppear = AnimationUtils.loadAnimation(getContext(), R.anim.slowly_fadein);

        btnNext1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                viewList.get(viewList.size() - 1).setVisibility(View.INVISIBLE);
                RxBus.get().send(Constants.RX_BUS_EVENT_NEXT_PAGE);
            }
        });

        btnNext2.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                viewList.get(viewList.size() - 1).setVisibility(View.INVISIBLE);
                RxBus.get().send(Constants.RX_BUS_EVENT_NEXT_PAGE);
            }
        });

        btnStart.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                RxBus.get().send(Constants.RX_BUS_EVENT_CLOSE);
            }
        });

        switch (position) {
            case 0:
                viewList.add(overlayBlack1);
                viewList.add(layoutGuide11);
                viewList.add(layoutGuide12);
                break;
            case 1:
                viewList.add(overlayBlack2);
                viewList.add(layoutGuide21);
                viewList.add(layoutGuide22);
                viewList.add(layoutGuide23);
                viewList.add(layoutGuide24);
                break;
            case 2:
                viewList.add(overlayBlack3);
                viewList.add(layoutGuide31);
                viewList.add(layoutGuide32);
                viewList.add(layoutGuide33);
                viewList.add(layoutGuide34);
                break;

        }

        for (View view : viewList) {
            view.setVisibility(View.INVISIBLE);
        }

        View[] layoutArray = {layoutGuide1, layoutGuide2, layoutGuide3};

        for (View view : layoutArray) {
            ViewUtil.setVisibility(view, view == layoutArray[position]);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        runGuide(0);
    }

    @Override
    public void onPause() {
        super.onPause();
        guideHandler.removeMessages(0);
        for (View view : viewList) {
            view.setVisibility(View.INVISIBLE);
        }
    }

    public List<View> getViewList() {
        return viewList;
    }

    public Animation getAniSlowlyAppear() {
        return aniSlowlyAppear;
    }

    public Animation getAniSlowlyDisappear() {
        return aniSlowlyDisappear;
    }

    private static class GuideHandler extends Handler {

        private final WeakReference<GuideFragment> guideFragment;

        public GuideHandler(GuideFragment guideFragment) {
            this.guideFragment = new WeakReference<>(guideFragment);
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);

            GuideFragment fragment = guideFragment.get();
            if (fragment != null) {

                int index = msg.arg1;

                List<View> viewList = fragment.getViewList();

                View currentView = viewList.get(index);
                currentView.setVisibility(View.VISIBLE);
                currentView.startAnimation(fragment.getAniSlowlyAppear());

                if (index < viewList.size() - 1) {
                    fragment.runGuide(index + 1);

                } else {
                    int i = 0;
                    for (View view : fragment.getViewList()) {
                        if (view != currentView && i > 0) {
                            view.setVisibility(View.INVISIBLE);
                            view.startAnimation(fragment.getAniSlowlyDisappear());
                        }
                        i++;
                    }
                }
            }
        }
    }

    private final GuideHandler guideHandler = new GuideHandler(this);

    public void runGuide(int index) {

        long delay = 1000L;

        if (index == viewList.size() - 1) {
            delay = 2000L;
        }

        Message msg = guideHandler.obtainMessage(0);
        msg.arg1 = index;

        guideHandler.sendMessageDelayed(msg, delay);
    }
}
