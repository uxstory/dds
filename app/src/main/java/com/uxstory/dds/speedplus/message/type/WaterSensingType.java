package com.uxstory.dds.speedplus.message.type;

public enum WaterSensingType {
    NOPE("nope"),
    BUSY("busy"),
    ADEQUACY("adequacy"),
    SHORTAGE("shortage"),
    ;

    private String value;

    WaterSensingType(String value) {
        this.value = value;
    }

    public String getString(){
        return value;
    }

    public static WaterSensingType getType(String type) {
        for (WaterSensingType state : WaterSensingType.values()) {
            if (state.getString().equals(type)) {
                return state;
            }
        }
        return NOPE;
    }
}
