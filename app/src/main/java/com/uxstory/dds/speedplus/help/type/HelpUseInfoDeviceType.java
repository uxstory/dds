package com.uxstory.dds.speedplus.help.type;

import android.content.Context;

import com.uxstory.dds.speedplus.R;

public enum HelpUseInfoDeviceType {

    INSTALL(R.drawable.icon_info_product, R.string.txt_use_info_device_category_install, R.array.help_device_install_array),
    MILLING(R.drawable.icon_info_work, R.string.txt_use_info_device_category_milling,  R.array.help_device_milling_array),
    WATER(R.drawable.icon_info_water, R.string.txt_use_info_device_category_water, R.array.help_device_water_array),
    BUR(R.drawable.icon_info_bur, R.string.txt_use_info_device_category_bur, R.array.help_device_bur_array),
    DEVICE(R.drawable.icon_info_repair, R.string.txt_use_info_device_category_device, R.array.help_device_repair_array),
    PROBLEM(R.drawable.icon_info_problem, R.string.txt_use_info_device_category_problem, R.array.help_device_problem_array),
    ;

    private int icon;
    private int string;
    private int contentsArray;

    HelpUseInfoDeviceType(int icon, int string, int contentsArray) {
        this.icon = icon;
        this.string = string;
        this.contentsArray = contentsArray;
    }

    public int getIcon() {
        return icon;
    }

    public int getString() {
        return string;
    }

    public String[] getContentsArray(Context context) {
        return context.getResources().getStringArray(contentsArray);
    }
}
