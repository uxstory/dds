package com.uxstory.dds.speedplus.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.viewpager2.widget.ViewPager2;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.adpater.ChangeGuideRecycleAdapter;
import com.uxstory.dds.speedplus.activity.ui.OnSingleClickListener;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.util.LogU;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangeWaterGuideActivity extends AbstractBaseActivity {

    private final static String TAG = ChangeWaterGuideActivity.class.getSimpleName();

    @BindView(R.id.view_close)
    View viewClose;

    @BindView(R.id.img_next)
    ImageView imgNext;

    @BindView(R.id.pager)
    ViewPager2 pager;

    @BindView(R.id.msg_1)
    TextView msg1;

    @BindView(R.id.msg_2)
    TextView msg2;

    @BindView(R.id.img_navi_dot_1)
    ImageView imgNaviDot1;

    @BindView(R.id.img_navi_dot_2)
    ImageView imgNaviDot2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int layoutId;
        if(ViewUtil.isKoreanLanguage()) {
            layoutId = R.layout.activity_change_water_guide;
        } else {
            layoutId = R.layout.activity_change_water_guide_en;
        }

        setContentView(layoutId);
        ButterKnife.bind(this);

        int[] imgGuideArray = {R.drawable.img_guide_water_01, R.drawable.img_guide_water_02};
        int[] msg1GuideArray = {R.string.txt_water_change_msg_1, R.string.txt_water_change_msg_2};
        int[] msg2GuideArray = {R.string.txt_msg_water_replace_guide, R.string.txt_msg_water_replace_guide};

        ImageView[] naviDots = {imgNaviDot1, imgNaviDot2};

        viewClose.setOnClickListener(v -> finish());

        imgNext.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                int i = pager.getCurrentItem() + 1;
                i = (i >= imgGuideArray.length) ? imgGuideArray.length - 1 : i;
                pager.setCurrentItem(i, true);
            }
        });

        ChangeGuideRecycleAdapter adapter = new ChangeGuideRecycleAdapter(this, imgGuideArray);
        pager.setAdapter(adapter);

        pager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                LogU.d(TAG, "onPageSelected()=" + position);
                ViewUtil.setVisibility(imgNext, position < imgGuideArray.length -1);

                msg1.setText(msg1GuideArray[position]);
                msg1.requestLayout();
                msg2.setText(msg2GuideArray[position]);
                msg2.requestLayout();

                ImageView imgDot = naviDots[position];
                for(ImageView view : naviDots) {
                    view.setSelected(view == imgDot);
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


}
