package com.uxstory.dds.speedplus.notification.type;

import com.uxstory.dds.speedplus.message.type.EventType;

public enum ErrorType {

    NULL(""),
    ERR_DOOR_OPEN(EventType.DOOR_OPEN.getString()),
    ERR_CAM_DISCONNECTED(EventType.CAM_DISCONNECTED.getString()),
    ERR_SENSING(EventType.ERR_SENSING.getString()),
    ERR_NOT_WORK(EventType.NOT_WORK.getString()),
    ERR_COLLET_OPEN(EventType.COLLET_OPEN.getString()),
    ERR_DIVICE_DISCONNECT(AppNotificationType.DEVICE_DISCONNECTED.getString()),
    ;

    private String value;

    ErrorType(String value) {
        this.value = value;
    }

    public String getString() {
        return value;
    }

    public static ErrorType getType(String type) {
        for (ErrorType errorType : ErrorType.values()) {
            if (errorType.getString().equals(type)) {
                return errorType;
            }
        }
        return NULL;
    }
}
