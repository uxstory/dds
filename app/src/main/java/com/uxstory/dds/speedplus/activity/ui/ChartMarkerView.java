package com.uxstory.dds.speedplus.activity.ui;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.ssomai.android.scalablelayout.ScalableLayout;
import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.model.DataCountModel;
import com.uxstory.dds.speedplus.util.LogU;

import java.util.ArrayList;
import java.util.Random;


public class ChartMarkerView extends MarkerView {

    private final static String TAG = ChartMarkerView.class.getSimpleName();

    private final int scaleWidth = 185;
    private final int viewWidth = (int) (scaleWidth * 2.25);

    public ChartMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);
        LinearLayout view = findViewById(R.id.layout_root);

        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
        lp.width = viewWidth;
        view.setLayoutParams(lp);
    }

    @Override
    public void refreshContent(Entry e, Highlight highlight) {
        // this will perform necessary layouting
        super.refreshContent(e, highlight);
    }

    public int getViewWidth() {
        return viewWidth;
    }

    //    @Override
//    public MPPointF getOffset() {
//        if(mOffset == null) {
//            mOffset = new MPPointF(10 * GlobalApplication.getDensity(), 0);
//        }
//
//        return mOffset;
//    }

    public int getRandomColor() {
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }

    public void setData(ArrayList<DataCountModel> list, ArrayList<LegendEntry> legendEntries) {
        ViewGroup viewGroup = findViewById(R.id.layout_body);
        viewGroup.removeAllViews();

        int total = 0;
        for (DataCountModel model : list) {
            int color = getRandomColor();
            for (LegendEntry legendEntry : legendEntries) {
                if (legendEntry.label.equals(model.getName())) {
                    color = legendEntry.formColor;
                    break;
                }
            }
            render(getContext(), viewGroup, model.getName(), model.getCount(), color);
            total += model.getCount();
        }

        TextView txtTotal = findViewById(R.id.txt_total);
        txtTotal.setText(String.format(getContext().getString(R.string.txt_total_milling_count), String.valueOf(total)));

        LinearLayout view = findViewById(R.id.layout_root);

        Rect outRect = new Rect();
        view.getGlobalVisibleRect(outRect);

        LogU.d(TAG, "refreshContent() x=" + outRect.left + ", y=" + outRect.top);
    }

    public void render(Context context, ViewGroup layoutBody, String name, int count, int color) {
        ScalableLayout sl = new ScalableLayout(context, scaleWidth, 22f);

        CardView cardView = new CardView(getContext());
        cardView.setCardBackgroundColor(color);
        cardView.setRadius(10);

        sl.addView(cardView, 10f, 0, 10f, 10f);
        ((ScalableLayout.LayoutParams) cardView.getLayoutParams()).gravity = Gravity.CENTER_VERTICAL;


        TextView tv = new TextView(context);
//        tv.setGravity(Gravity.CENTER_HORIZONTAL);

        String val = name + " : {#" + count + "#}";

        ViewUtil.highlightTagText(getContext(), tv, val, 0);

        sl.addView(tv, 26f, 0f, 0f, 18f);
        sl.setTextView_WrapContent(tv, ScalableLayout.TextView_WrapContent_Direction.Right, false);
        sl.setScale_TextSize(tv, 12);
        ((ScalableLayout.LayoutParams) tv.getLayoutParams()).gravity = Gravity.CENTER_VERTICAL;

        layoutBody.addView(sl);

        cardView.requestLayout();
        tv.requestLayout();

    }
}
