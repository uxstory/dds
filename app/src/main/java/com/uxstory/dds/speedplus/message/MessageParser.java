package com.uxstory.dds.speedplus.message;

import android.net.Uri;

import com.uxstory.dds.speedplus.database.DbOpenHelper;
import com.uxstory.dds.speedplus.database.DbQuery;
import com.uxstory.dds.speedplus.device.BurInfo;
import com.uxstory.dds.speedplus.device.Device;
import com.uxstory.dds.speedplus.device.Event;
import com.uxstory.dds.speedplus.device.MillingProgress;
import com.uxstory.dds.speedplus.message.type.ResponseType;
import com.uxstory.dds.speedplus.message.type.XmlTagType;
import com.uxstory.dds.speedplus.notification.Notification;
import com.uxstory.dds.speedplus.notification.NotificationPublisher;
import com.uxstory.dds.speedplus.util.LogU;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

public class MessageParser {

    final static String TAG = MessageParser.class.getSimpleName();

    public static String getText(String tag, String xml) {

        try {
            XmlPullParserFactory parserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = parserFactory.newPullParser();

            parser.setInput(new StringReader(xml));

            boolean isMatch = false;
            int eventType = parser.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_DOCUMENT) {
                    // XML 데이터 시작
                } else if (eventType == XmlPullParser.START_TAG) {
                    String startTag = parser.getName();
                    if (startTag.equals(tag)) {
                        isMatch = true;
                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    //String endTag = parser.getName();
                } else if (eventType == XmlPullParser.TEXT) {
                    if (isMatch) {
                        return parser.getText();
                    }
                }

                eventType = parser.next();
            }

        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String getValue(String tag, String name, String xml) {

        try {
            XmlPullParserFactory parserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = parserFactory.newPullParser();

            parser.setInput(new StringReader(xml));

            int eventType = parser.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_DOCUMENT) {
                    // XML 데이터 시작
                } else if (eventType == XmlPullParser.START_TAG) {
                    if (parser.getName().equals(tag)) {
                        return parser.getAttributeValue(null, name);
                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    //String endTag = parser.getName();

                } else if (eventType == XmlPullParser.TEXT) {

                }

                eventType = parser.next();
            }

        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static XmlTagType getXmlTagType(String xml) {

        try {
            XmlPullParserFactory parserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = parserFactory.newPullParser();

            parser.setInput(new StringReader(xml));

            int eventType = parser.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_DOCUMENT) {
                    // XML 데이터 시작
                } else if (eventType == XmlPullParser.START_TAG) {
                    String startTag = parser.getName();
                    XmlTagType xmlTagType = XmlTagType.getType(startTag);
                    if(xmlTagType != XmlTagType.NULL) {
                        return xmlTagType;
                    }

                } else if (eventType == XmlPullParser.END_TAG) {
                    //String endTag = parser.getName();

                } else if (eventType == XmlPullParser.TEXT) {

                }

                eventType = parser.next();
            }

        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
        }

        return XmlTagType.NULL;
    }

    public static ResponseType getResponseType(String xml) {

        try {
            XmlPullParserFactory parserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = parserFactory.newPullParser();

            parser.setInput(new StringReader(xml));

            int eventType = parser.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_DOCUMENT) {
                    // XML 데이터 시작
                } else if (eventType == XmlPullParser.START_TAG) {
                    if (parser.getName().equals(XmlTagType.COMMAND_RESPONSE.getString())) {
                        return ResponseType.getType(parser.getAttributeValue(null, XmlAttr.NAME));
                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    //String endTag = parser.getName();

                } else if (eventType == XmlPullParser.TEXT) {

                }

                eventType = parser.next();
            }

        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
        }

        return ResponseType.NULL;
    }

    public static String getResponseResult(ResponseType responseType, String xml) {

        try {
            XmlPullParserFactory parserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = parserFactory.newPullParser();

            parser.setInput(new StringReader(xml));

            int eventType = parser.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_DOCUMENT) {
                    // XML 데이터 시작
                } else if (eventType == XmlPullParser.START_TAG) {
                    if (parser.getName().equals(XmlTagType.COMMAND_RESPONSE.getString())) {
                        if (responseType.getString().equals(parser.getAttributeValue(null, XmlAttr.NAME))) {
                            return parser.getAttributeValue(null, XmlAttr.RESPONSE);
                        }
                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    //String endTag = parser.getName();

                } else if (eventType == XmlPullParser.TEXT) {

                }

                eventType = parser.next();
            }

        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
        }

        return null;
    }


    public static void parseInsertEventHistory(Device device, String xml) {

//        ArrayList<Event> eventArrayList = new ArrayList<>();

        try {
            XmlPullParserFactory parserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = parserFactory.newPullParser();

            parser.setInput(new StringReader(xml));

            int eventType = parser.getEventType();
            Event event = null;
            Event millingStartEvent = null;

            DbOpenHelper.DB.beginTransaction();

            while (eventType != XmlPullParser.END_DOCUMENT) {

//                long startTime = System.nanoTime();
                event = parseEvent(event, parser);
//                long finishTime = System.nanoTime();
//                LogU.e(TAG, "Parse 1 Event takes: "+ (finishTime-startTime)/1e9 +" seconds");

                if(event != null) {
//                    eventArrayList.add(event);

                    if (EventChecker.isMillingStartEvent(event)) {
                        millingStartEvent = event;

                    } else if (EventChecker.isMillingReStartEvent(event) && millingStartEvent != null) {
                        millingStartEvent.setMillingStartTime(event.getMillingStartTime());

                    } else if (EventChecker.isMillingEndEvent(event) && millingStartEvent != null) {
                        event.setPatientName(millingStartEvent.getPatientName());
                        event.setBlock(millingStartEvent.getBlock());
                        event.setRestoration(millingStartEvent.getRestoration());
                        event.setLeftBurName(millingStartEvent.getLeftBurName());
                        event.setRightBurName(millingStartEvent.getRightBurName());
                        event.setMillingStartTime(millingStartEvent.getTime());
                    }
                    if (EventChecker.isMillingStopEvent(event)) {
                        millingStartEvent = null;
                    }

//                    startTime = System.nanoTime();
                    long result = DbQuery.insertEvent(device.getSerial(), event, true);
//                    finishTime = System.nanoTime();
//                    LogU.e(TAG, "Write to DB 1 Event takes: "+ (finishTime-startTime)/1e9 +" seconds");

                    //TODO test
//                    if(result > -1) {
//                        Notification notification = NotificationPublisher.getNotification(device, event);
//                        if (notification != null) {
//                            DbQuery.insertNotification(device.getSerial(), notification);
//                        }
//                    }
                }
                eventType = parser.next();
            }

            DbOpenHelper.DB.setTransactionSuccessful();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DbOpenHelper.DB.endTransaction();
        }

//        return eventArrayList;

    }

    public static Event getEvent(String xml) {

        Event event = null;
        try {
            XmlPullParserFactory parserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = parserFactory.newPullParser();

            parser.setInput(new StringReader(xml));

//            int eventType = parser.getType();
//            while (eventType != XmlPullParser.END_DOCUMENT) {
//                event = parseEvent(event, parser);
//                eventType = parser.next();
//            }

            return parseEvent(event, parser);

        } catch (Exception e) {
            e.printStackTrace();
            event = null;
        }

        return event;
    }

    private static Event parseEvent(Event event, XmlPullParser parser) throws Exception {

        int eventType = parser.getEventType();

        if (eventType == XmlPullParser.START_TAG) {
            String startTag = parser.getName();
            if (startTag.equals(XmlTagType.EVENT.getString())) {
                event = new Event();
                event.setType(parser.getAttributeValue(null, XmlAttr.TYPE));
                event.setTime(parser.getAttributeValue(null, XmlAttr.TIME));
                if(event.getTime() == null) {
                    event.setTime(parser.getAttributeValue(null, XmlAttr.DATE));
                }
                event.setTime(event.getTime().replace('T', ' '));

                String uri = parser.getAttributeValue(null, XmlAttr.URI);
                event.setUri(uri);
                Uri parsedUri = Uri.parse(event.getUri());
                List<String> segments = parsedUri.getPathSegments();
                event.setBootIndex(Integer.parseInt(segments.get(2)));
                event.setWorkStep(Integer.parseInt(segments.get(5)));

            } else if (startTag.equals(XmlAttr.WORK_INFO)) {
                event.setPatientName(parser.getAttributeValue(null, XmlAttr.PATIENT_NAME));
                event.setBlock(parser.getAttributeValue(null, XmlAttr.BLOCK));
                event.setRestoration(parser.getAttributeValue(null, XmlAttr.RESTORATION));
                event.setLeftBurName(parser.getAttributeValue(null, XmlAttr.LEFT_BUR_NAME));
                event.setRightBurName(parser.getAttributeValue(null, XmlAttr.RIGHT_BUR_NAME));

            } else if (startTag.equals(XmlAttr.WORK_FINISH)) {
                event.setElapsedTime(parser.getAttributeValue(null, XmlAttr.ELAPSED_TIME));
            }

        } else if (eventType == XmlPullParser.END_TAG) {
            return event;

        } else if (eventType == XmlPullParser.TEXT) {

        }

        parser.next();

        return parseEvent(event, parser);
    }

    public static MillingProgress getMillingProgress(String xml) {

        try {
            XmlPullParserFactory parserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = parserFactory.newPullParser();

            parser.setInput(new StringReader(xml));

            int eventType = parser.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_DOCUMENT) {
                    // XML 데이터 시작
                } else if (eventType == XmlPullParser.START_TAG) {
                    String startTag = parser.getName();
                    if (startTag.equals(ResponseType.MILLING_PROGRESS.getString())) {
                        MillingProgress millingProgress = new MillingProgress();
                        millingProgress.setPercent(Integer.parseInt(parser.getAttributeValue(null, XmlAttr.PERCENT)));
                        millingProgress.setRemainingTime(parser.getAttributeValue(null, XmlAttr.REMAINING_TIME));
                        return millingProgress;
                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    //String endTag = parser.getName();
                } else if (eventType == XmlPullParser.TEXT) {

                }

                eventType = parser.next();
            }

        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static BurInfo getBurInfo(String xml) {

        try {
            XmlPullParserFactory parserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = parserFactory.newPullParser();

            parser.setInput(new StringReader(xml));

            int eventType = parser.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_DOCUMENT) {
                    // XML 데이터 시작
                } else if (eventType == XmlPullParser.START_TAG) {
                    String startTag = parser.getName();
                    if (startTag.equals(ResponseType.BUR_INFO.getString())) {
                        BurInfo burInfo = new BurInfo();
                        burInfo.setLeftBurName(parser.getAttributeValue(null, XmlAttr.LEFT_BUR_NAME));
                        burInfo.setRightBurName(parser.getAttributeValue(null, XmlAttr.RIGHT_BUR_NAME));
                        burInfo.setLeftBurUsingCount(Integer.parseInt(parser.getAttributeValue(null, XmlAttr.LEFT_BUR_USING_COUNT)));
                        burInfo.setRightBurUsingCount(Integer.parseInt(parser.getAttributeValue(null, XmlAttr.RIGHT_BUR_USING_COUNT)));
                        return burInfo;
                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    //String endTag = parser.getName();
                } else if (eventType == XmlPullParser.TEXT) {

                }

                eventType = parser.next();
            }

        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
        }

        return null;
    }
}
