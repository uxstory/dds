package com.uxstory.dds.speedplus.activity.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.adpater.DataCountRecycleAdapter;
import com.uxstory.dds.speedplus.database.DbQuery;
import com.uxstory.dds.speedplus.device.Device;
import com.uxstory.dds.speedplus.model.ComparatorDataCount;
import com.uxstory.dds.speedplus.model.DataCountModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class HistoryStatisticsPageDetailFragment extends AbstractBaseFragment {

    private final static String TAG = HistoryStatisticsPageDetailFragment.class.getSimpleName();

    @BindView(R.id.view_milling_history)
    View viewMillingHistory;

    @BindView(R.id.view_no_milling_history)
    View viewNoMillingHistory;

    @BindView(R.id.recycler_block)
    RecyclerView recyclerBlock;

    @BindView(R.id.recycler_restoration)
    RecyclerView recyclerRestoration;

    @BindView(R.id.recycler_bur)
    RecyclerView recyclerBur;

    private List<DataCountModel> blockList = new ArrayList<>();
    private DataCountRecycleAdapter recyclerAdapterBlock;

    private List<DataCountModel> restorationList = new ArrayList<>();
    private DataCountRecycleAdapter recyclerAdapterRestoration;

    private List<DataCountModel> burList = new ArrayList<>();
    private DataCountRecycleAdapter recyclerAdapterBur;


    public static HistoryStatisticsPageDetailFragment newInstance(Device device, DataCountModel dataCountModel) {
        HistoryStatisticsPageDetailFragment fragment = new HistoryStatisticsPageDetailFragment();
        Bundle args = new Bundle();
        args.putString("date", dataCountModel.getTime());
        args.putString("serial", device.getSerial());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        setLayoutId(R.layout.fragment_history_statistics_page_detail);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        recyclerBlock.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerAdapterBlock = new DataCountRecycleAdapter(getActivity(), blockList);
        recyclerBlock.setAdapter(recyclerAdapterBlock);


        recyclerRestoration.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerAdapterRestoration = new DataCountRecycleAdapter(getActivity(), restorationList);
        recyclerRestoration.setAdapter(recyclerAdapterRestoration);

        recyclerBur.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerAdapterBur = new DataCountRecycleAdapter(getActivity(), burList);
        recyclerBur.setAdapter(recyclerAdapterBur);

        updateView();
    }

    public void updateView() {
        String date = getArguments() != null ? getArguments().getString("date") : "";
        String serial = getArguments() != null ? getArguments().getString("serial") : "";
        setMillingData(serial, date);
    }

    private void setMillingData(String serial, String date) {

        int totalCount = DbQuery.selectMillingFinishCount(serial, date);

        if (totalCount > 0) {

            viewMillingHistory.setVisibility(View.VISIBLE);
            viewNoMillingHistory.setVisibility(View.GONE);

            blockList = DbQuery.selectBlockCountList(serial, date);
            recyclerAdapterBlock.setList(blockList);
            recyclerAdapterBlock.notifyDataSetChanged();

            restorationList = DbQuery.selectRestorationCountList(serial, date);
            recyclerAdapterRestoration.setList(restorationList);
            recyclerAdapterRestoration.notifyDataSetChanged();

            burList = DbQuery.selectBurCountList(serial, date);
            burList.sort(new ComparatorDataCount());

            recyclerAdapterBur.setList(burList);
            recyclerAdapterBur.notifyDataSetChanged();

        } else {
            viewMillingHistory.setVisibility(View.GONE);
            viewNoMillingHistory.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onUpdateView() {

    }
}
