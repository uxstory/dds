package com.uxstory.dds.speedplus.activity.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.MainActivity;
import com.uxstory.dds.speedplus.activity.PairingActivity;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class NoDeviceFragment extends AbstractBaseFragment {

    private final static String TAG = NoDeviceFragment.class.getSimpleName();

    @BindView(R.id.btn_01)
    Button buttonAdd;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        setLayoutId(R.layout.fragment_no_device);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        buttonAdd.setOnClickListener(view1 -> {
            startActivityForResult(new Intent(getActivity(), PairingActivity.class), 0);
        });

        getMainAcivity().updateToolbarMain(R.id.menu_dash);
    }

    @Override
    protected void onUpdateView() {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == 0) {
            if(getActivity() != null && getDdsService() != null && getDdsService().getDeviceList().size() > 0) {
//                FragmentTransaction transaction = fragmentManager.beginTransaction();
//                fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                ((MainActivity)getActivity()).goToMenu(R.id.menu_dash);
            }
        }

    }
}
