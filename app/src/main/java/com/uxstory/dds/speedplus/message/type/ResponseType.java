package com.uxstory.dds.speedplus.message.type;

public enum ResponseType {

    NULL(""),
    RECONNECT(RequestType.RECONNECT.getString()),
    CONNECTED("<connected>"),
    DISCONNECTED("<disconnected>"),

    SESSION(RequestType.SESSION.getString()),
    SYNC_EVENT_HISTORY(RequestType.EVENT_HISTORY.getString()),
    SYNC_EVENT_HISTORY_ERROR("<eventHistoryError>"),
    WATER_SENSING("water_sensing"),
    MILLING_PROGRESS(RequestType.MILLING_PROGRESS.getString()),
    ECHO_KEEP_ALIVE(RequestType.ECHO_KEEP_ALIVE.getString()),
    BUR_INFO(RequestType.BUR_INFO.getString()),
    UNPAIRING(RequestType.UNPAIRING.getString()),
    UNPAIRING_ERROR("<connectCompleteError>"),
    EVENT("<event>"),
    CHANGED_DEVICE_LIST("<changedDeviceList>"),
    ;

    private String value;

    ResponseType(String value) {
        this.value = value;
    }

    public String getString() {
        return value;
    }

    public static ResponseType getType(String responseName) {
        for (ResponseType responseType : ResponseType.values()) {
            if (responseType.getString().equals(responseName)) {
                return responseType;
            }
        }
        return NULL;
    }
}
