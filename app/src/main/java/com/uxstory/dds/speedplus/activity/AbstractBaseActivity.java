package com.uxstory.dds.speedplus.activity;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.uxstory.dds.speedplus.application.GlobalApplication;
import com.uxstory.dds.speedplus.service.DdsService;
import com.uxstory.dds.speedplus.util.LogU;

import butterknife.ButterKnife;
import pe.warrenth.rxbus2.OnRxBusFindDataInterface;


public abstract class AbstractBaseActivity extends AppCompatActivity implements OnRxBusFindDataInterface {

    private final static String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected DdsService getDdsService() {
        return GlobalApplication.getDdsService();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LogU.d(TAG, "onConfigurationChanged() " + this);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public interface onKeyBackPressedListener {
        void onBackPressed();
    }

    public onKeyBackPressedListener mOnKeyBackPressedListener;

    public void setOnKeyBackPressedListener(onKeyBackPressedListener listener) {
        mOnKeyBackPressedListener = listener;
    }

    @Override
    public void onBackPressed() {
        if (mOnKeyBackPressedListener != null) {
            mOnKeyBackPressedListener.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public Object getObject() {
        return this;
    }

    @Override
    public int getHashCode() {
        return System.identityHashCode(this);
    }

    /**
     * EditText focus out for Fragment click
     * @param event
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();

            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
}
