package com.uxstory.dds.speedplus.activity.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.util.LogU;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangeGuideRecycleAdapter extends RecyclerView.Adapter<ChangeGuideRecycleAdapter.ViewHolder> {

    private final static String TAG = ChangeGuideRecycleAdapter.class.getSimpleName();

    private Context context;
    private int[] items;

    public ChangeGuideRecycleAdapter(Context context, int[] items) {
        this.context = context;
        this.items = items;
    }

    public void setList(int[] items) {
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_change_guide, parent, false);
        v.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LogU.d(TAG, "onBindViewHolder() position=" + position);

        int imgId = items[position];
        holder.imageGuide.setImageResource(imgId);
    }

    @Override
    public int getItemCount() {
        return this.items.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_guide)
        ImageView imageGuide;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
