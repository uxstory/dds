package com.uxstory.dds.speedplus.activity.adpater;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.ui.OnSingleClickListener;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.application.GlobalApplication;
import com.uxstory.dds.speedplus.communicator.CommunicatorEvent;
import com.uxstory.dds.speedplus.config.Constants;
import com.uxstory.dds.speedplus.database.DbQuery;
import com.uxstory.dds.speedplus.device.Device;
import com.uxstory.dds.speedplus.message.type.ResponseType;
import com.uxstory.dds.speedplus.message.type.EventHistorySyncType;
import com.uxstory.dds.speedplus.util.LogU;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pe.warrenth.rxbus2.RxBus;

public class SettingDeviceRecycleAdapter extends RecyclerView.Adapter<SettingDeviceRecycleAdapter.ViewHolder> {

    private final static String TAG = SettingDeviceRecycleAdapter.class.getSimpleName();

    private Context context;
    private List<Device> items;
//    private CommunicatorEventListener communicatorEventListener;

    private boolean isAllSync;

    public void setAllSync(boolean isAllSync) {
        this.isAllSync = isAllSync;
    }

    public boolean isAllSync() {
        return isAllSync;
    }

    public SettingDeviceRecycleAdapter(Context context, List<Device> items) {
        this.context = context;
        this.items = items;
//        this.communicatorEventListener = communicatorEventListener;
    }

    public void setList(List<Device> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_setting_device, parent, false);
        v.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LogU.d(TAG, "onBindViewHolder() position=" + position);

        Device device = items.get(position);
        String index = String.valueOf(position + 1);

        holder.txtProductNum.setText(index);
        holder.txtDeviceName.setText(device.getName());
        holder.editDeviceName.setText(device.getName());
        holder.txtSerial.setText(device.getSerial());

        String lastSyncDate = device.getLastEventHistorySyncDateA();
        if (lastSyncDate != null && !lastSyncDate.isEmpty()) {
            holder.txtSyncDate.setText(String.format(context.getString(R.string.txt_setting_pairing_date), lastSyncDate));
        } else {
            holder.txtSyncDate.setText("");
        }

        Device pairedDevice = GlobalApplication.getDdsService().getPairedDevice(device.getSerial());

        holder.btnSync.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if(GlobalApplication.getDdsService().requestHistory(pairedDevice)) {
                    setState(holder, pairedDevice);
                }
            }
        });

        holder.btnUnpairing.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                confirmUnPairing(holder, pairedDevice);
            }
        });

        setState(holder, pairedDevice);

        holder.txtProductNum.requestLayout();
        holder.txtDeviceName.requestLayout();
        holder.txtSerial.requestLayout();
        holder.txtSyncDate.requestLayout();

        android.os.Handler handler = new android.os.Handler();

        switch (holder.responseType) {
            case SYNC_EVENT_HISTORY:
                holder.txtNetworkState.setText(R.string.txt_setting_sync_succeed);
                handler.postDelayed(() -> {
                    holder.responseType = ResponseType.NULL;
                    setState(holder, pairedDevice);
                }, 2000);
                break;
            case SYNC_EVENT_HISTORY_ERROR:
                holder.txtNetworkState.setText(R.string.txt_setting_sync_fail);
                handler.postDelayed(() -> {
                    holder.responseType = ResponseType.NULL;
                    setState(holder, pairedDevice);
                }, 2000);
                break;
            case UNPAIRING:
                holder.txtNetworkState.setText(R.string.txt_setting_unpaired);
                handler.postDelayed(() -> {
                    holder.responseType = ResponseType.NULL;
                    items.remove(position);
                    if(items.size() > 0) {
                        SettingDeviceRecycleAdapter.this.notifyItemRemoved(position);
//                        SettingDeviceRecycleAdapter.this.notifyItemRangeChanged(position, getItemCount());
                    } else {
                        //invoke SettingProductInfoFragment.onUpdateView()
                        RxBus.get().send(Constants.RX_BUS_EVENT_DEVICE, new CommunicatorEvent(ResponseType.NULL));
                    }
                }, 2000);
                break;
            case UNPAIRING_ERROR:
                holder.txtNetworkState.setText(R.string.txt_setting_unpaired_fail);
                handler.postDelayed(() -> {
                    holder.responseType = ResponseType.NULL;
                    setState(holder, pairedDevice);
                }, 2000);
                break;
        }

        holder.editDeviceName.setOnFocusChangeListener((v, hasFocus) -> {
            if(!hasFocus) {
                holder.txtDeviceName.setVisibility(View.VISIBLE);
                holder.editDeviceName.setVisibility(View.GONE);
                holder.editDeviceName.setText(device.getName());
            }
        });

        holder.editDeviceName.setOnEditorActionListener((v, actionId, event) -> {
            onEditDeviceName(holder, device);
            return true;
        });

//        ViewUtil.requestLayout((ViewGroup) holder.itemView);
    }

    private void onEditDeviceName(ViewHolder holder, Device device) {
        String deviceName = holder.editDeviceName.getText().toString();
        for(Device d : items) {
            if(d != device && d.getName().equals(deviceName)) {
                alertDupDeviceName(d);
                return;
            }
        }
        DbQuery.updateDeviceName(device.getSerial(), deviceName);
        GlobalApplication.getDdsService().updateDeviceName(device.getSerial(), deviceName);
        holder.txtDeviceName.setText(deviceName);
        holder.txtDeviceName.setVisibility(View.VISIBLE);
        holder.editDeviceName.setVisibility(View.GONE);
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if(imm != null) {
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        }
    }

    private void alertDupDeviceName(Device device) {
        AlertDialog.Builder builder = ViewUtil.getDialogBuilder(context, device.getName(), R.string.txt_setting_dup_device_name, R.drawable.icon_noti_error);
        builder.setPositiveButton(R.string.txt_confirm, (dialog, i) -> dialog.dismiss());
        ViewUtil.showDialog(context, builder);
    }

    private void setState(ViewHolder holder, Device pairedDevice) {

        if (pairedDevice != null) {
            holder.imgProductLink.setEnabled(pairedDevice.isConnected());

            if (pairedDevice.getEventHistorySyncType() == EventHistorySyncType.REQUEST) {
                holder.layoutBtnSyncing.setVisibility(View.VISIBLE);
                holder.btnSync.setVisibility(View.INVISIBLE);
                holder.txtNetworkState.setText(R.string.txt_setting_connected);
                setEnabledButtons(holder, false);

            } else if (pairedDevice.getWorkStateType().isMillingState()) {
                holder.layoutBtnSyncing.setVisibility(View.INVISIBLE);
                holder.btnSync.setVisibility(View.VISIBLE);
                holder.txtNetworkState.setText(R.string.txt_milling_ing);
                setEnabledButtons(holder, false);

            } else {
                holder.layoutBtnSyncing.setVisibility(View.INVISIBLE);
                holder.btnSync.setVisibility(View.VISIBLE);
                holder.txtNetworkState.setText(pairedDevice.isConnected() ? R.string.txt_setting_connected : R.string.txt_setting_no_connected);
                setEnabledButtons(holder, !isAllSync && pairedDevice.isConnected());
            }

        } else {
            holder.imgProductLink.setEnabled(false);
            holder.txtNetworkState.setText(R.string.txt_setting_unpaired);
            holder.layoutBtnSyncing.setVisibility(View.INVISIBLE);
            holder.btnSync.setVisibility(View.VISIBLE);
            setEnabledButtons(holder, false);
        }
    }

    private void setEnabledButtons(ViewHolder holder, boolean enabled) {
        holder.btnSync.setEnabled(enabled);
        holder.btnUnpairing.setEnabled(enabled);
    }

    private void confirmUnPairing(ViewHolder holder, Device pairedDevice) {
        AlertDialog.Builder builder = ViewUtil.getDialogBuilder(context, pairedDevice.getName(), R.string.txt_setting_unpairing_confirm_msg, R.drawable.icon_noti_00);
        builder.setPositiveButton(R.string.txt_confirm, (dialog, i) -> {
            GlobalApplication.getDdsService().requestUnPairing(pairedDevice);
            setEnabledButtons(holder, false);
        });
        builder.setNegativeButton(R.string.txt_cancel, (dialog, i) -> dialog.dismiss());
        ViewUtil.showDialog(context, builder);
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.txt_product_num)
        TextView txtProductNum;

        @BindView(R.id.txt_device_name)
        TextView txtDeviceName;


        @BindView(R.id.edit_device_name)
        EditText editDeviceName;

        @BindView(R.id.txt_serial)
        TextView txtSerial;

        @BindView(R.id.layout_btn_syncing)
        View layoutBtnSyncing;

        @BindView(R.id.img_product_link)
        ImageView imgProductLink;

        @BindView(R.id.txt_network_state)
        TextView txtNetworkState;

        @BindView(R.id.txt_sync_date)
        TextView txtSyncDate;

        @BindView(R.id.btn_sync)
        Button btnSync;

        @BindView(R.id.btn_unpairing)
        View btnUnpairing;

        public ResponseType responseType = ResponseType.NULL;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            txtDeviceName.setOnLongClickListener(v -> {
                txtDeviceName.setVisibility(View.GONE);
                editDeviceName.setVisibility(View.VISIBLE);
                editDeviceName.requestFocus();
                editDeviceName.setSelection(editDeviceName.getText().length());
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                if(imm != null) {
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }
                return false;
            });
        }
    }
}
