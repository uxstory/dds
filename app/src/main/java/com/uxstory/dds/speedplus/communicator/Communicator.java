package com.uxstory.dds.speedplus.communicator;

import com.uxstory.dds.speedplus.message.type.RequestType;

import java.io.IOException;

public interface Communicator {

    void setCommunicatorEventListener(CommunicatorEventListener responseEventListener);

    void connect() throws IOException;

    void disconnect();

    void request(RequestType requestType);
}
