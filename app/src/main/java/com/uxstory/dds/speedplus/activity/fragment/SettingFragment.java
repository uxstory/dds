package com.uxstory.dds.speedplus.activity.fragment;


import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.ui.OnSingleClickListener;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends AbstractBaseFragment {

    private final static String TAG = SettingFragment.class.getSimpleName();

    private SettingProductInfoFragment settingProductInfoFragment = new SettingProductInfoFragment();
    private SettingNotificationFragment settingNotificationFragment = new SettingNotificationFragment();

    @BindView(R.id.setting_product)
    View viewSettingProduct;

    @BindView(R.id.setting_noti)
    View viewSettingNoti;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        setLayoutId(R.layout.fragment_setting);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        viewSettingProduct.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                addFragment(getActivity(), settingProductInfoFragment);
            }
        });

        viewSettingNoti.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                addFragment(getActivity(), settingNotificationFragment);
            }
        });

        onUpdateView();
    }

    @Override
    protected void onUpdateView() {
        getMainAcivity().updateToolbarMain(R.id.menu_setting);
    }
}
