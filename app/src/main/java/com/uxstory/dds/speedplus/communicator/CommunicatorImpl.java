package com.uxstory.dds.speedplus.communicator;

import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;

import com.uxstory.dds.speedplus.BuildConfig;
import com.uxstory.dds.speedplus.database.DbQuery;
import com.uxstory.dds.speedplus.device.Device;
import com.uxstory.dds.speedplus.device.Event;
import com.uxstory.dds.speedplus.message.MessageGenerator;
import com.uxstory.dds.speedplus.message.MessageParser;
import com.uxstory.dds.speedplus.message.MessageProcessor;
import com.uxstory.dds.speedplus.message.XmlAttr;
import com.uxstory.dds.speedplus.message.type.RequestType;
import com.uxstory.dds.speedplus.message.type.ResponseType;
import com.uxstory.dds.speedplus.message.type.WorkStateType;
import com.uxstory.dds.speedplus.message.type.XmlTagType;
import com.uxstory.dds.speedplus.util.LogU;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;

import static com.uxstory.dds.speedplus.message.MessageGenerator.HEADER_BODY_LENGTH_SIZE;
import static com.uxstory.dds.speedplus.message.MessageGenerator.HEADER_PACKET_TYPE_SIZE;
import static com.uxstory.dds.speedplus.message.MessageGenerator.PACKET_TYPE_ECHO_KEEP_ALIVE;

public class CommunicatorImpl extends Thread implements Communicator {

    private final static String TAG = CommunicatorImpl.class.getSimpleName();

    private boolean isDisconnect;

    private Device device;
    private String host;
    private int port;
    private Socket socket;
    private DataOutputStream dataOutputStream;
    private DataInputStream dataInputStream;
    private String session;
    private String encryptKey;

    private RequestHandler requestHandler;
    private ResponseHandler responseHandler;
    private CommunicatorEventListener communicatorEventListener;

    private long lastEchoKeepAliveRcvTime;

    public CommunicatorImpl(Device device, CommunicatorEventListener communicatorEventListener) {
        this.device = device;
        this.host = device.getIp();
        this.port = device.getPort();
        this.session = device.getSession();
        this.encryptKey = device.getEncryptKey();
        this.communicatorEventListener = communicatorEventListener;

        LogU.d(TAG, "network check connecting device=" + device.getSerial());
        LogU.d(TAG, "network check connecting ip=" + device.getIp());
    }

    @Override
    public void run() {

        try {
            synchronized (this) {
                if (isDisconnect) {
                    return;
                }

                socket = new Socket();
            }

            socket.connect(new InetSocketAddress(host, port), BuildConfig.SOCKET_CONNECT_TIMEOUT * 1000);

            OutputStream os = socket.getOutputStream();
            dataOutputStream = new DataOutputStream(os);

            InputStream is = socket.getInputStream();
            dataInputStream = new DataInputStream(is);

            connect();

        } catch (Exception e) {
//            e.printStackTrace();
            LogU.d(TAG, e.getMessage());

        } finally {
            disconnect();
        }
    }

    @Override
    public void setCommunicatorEventListener(CommunicatorEventListener responseEventListener) {
        this.communicatorEventListener = responseEventListener;
    }

    private boolean checkSerial(String xml) {
        String productUri = MessageParser.getValue(XmlTagType.DDS_CONTROL.getString(), XmlAttr.PRODUCT_URI, xml);
        Uri uri = Uri.parse(productUri);
        String serial = uri.getLastPathSegment();

        LogU.d(TAG, "checkSerial() device=" + device.getSerial() + ", try=" + serial);

        return device.getSerial().equals(serial);
    }

    @Override
    public void connect() throws IOException {
        LogU.d(TAG, "connect()");
        if (session != null && encryptKey != null) {
            reconnect();
        } else {
            connectInitSession();
        }
    }

    private void reconnect() throws IOException {
        LogU.d(TAG, "reconnect()");
        send(MessageGenerator.getRequestMessage(MessageGenerator.generateSessionKey(encryptKey, session), RequestType.RECONNECT));
        String xml = read();

        if (!checkSerial(xml)) {
            LogU.e(TAG, "checkSerial false!");

        } else {

            String connected = MessageParser.getResponseResult(ResponseType.RECONNECT, xml);
            if (XmlAttr.VALUE_OK.equals(connected)) {
                listeningEvent();

            } else {
                LogU.e(TAG, "reconnect() fail.");
                session = null;
                encryptKey = null;
                if (device.getWorkStateType() == WorkStateType.RETRY_CONNECTING) {
                    connectInitSession();
                }
            }
        }
    }

    private void connectInitSession() throws IOException {
        LogU.d(TAG, "connectInitSession()");

        send(MessageGenerator.getRequestSessionMessage());
        onReceiveEvent(ResponseType.SESSION);

        String xml = read();

        if (!checkSerial(xml)) {
            LogU.e(TAG, "checkSerial false!");

        } else {

            encryptKey = MessageParser.getText(XmlTagType.KEY.getString(), xml);
            LogU.d(TAG, "Recv encryptKey : " + encryptKey);

            if (encryptKey != null) {
                encryptKey = encryptKey.trim();
            }

            if (encryptKey != null && !"".equals(encryptKey)) {
                session = MessageGenerator.generateSession();
                send(MessageGenerator.getTransSessionMessage(MessageGenerator.generateSessionKey(encryptKey, session)));
                xml = read();
                String connected = MessageParser.getResponseResult(ResponseType.SESSION, xml);
                LogU.d(TAG, "connected : " + connected);

                if (XmlAttr.VALUE_OK.equals(connected)) {
                    device.setSession(session);
                    device.setEncryptKey(encryptKey);
                    DbQuery.updateDevice(device, true);

                    listeningEvent();

                } else {
                    LogU.e(TAG, "Session connect fail!");
                }
            }
        }
    }

    private void listeningEvent() throws IOException {
        LogU.d(TAG, "listeningEvent()");

        HandlerThread t1 = new HandlerThread("Request Handler Thread");
        t1.start();
        requestHandler = new RequestHandler(t1.getLooper());

        HandlerThread t2 = new HandlerThread("Receive Handler Thread");
        t2.start();
        responseHandler = new ResponseHandler(t2.getLooper());

        onReceiveEvent(ResponseType.CONNECTED);

        while (!isInterrupted()) {
            String xml = read();
            LogU.d(TAG, "listeningEvent() " + this);
            if (xml != null && !xml.isEmpty() && responseHandler != null) {
                Message msg = responseHandler.obtainMessage(RESPONSE, xml);
                responseHandler.sendMessage(msg);
            }
        }
    }

    private void checkEchoKeepAlive() {
        final int sleepTime = BuildConfig.INTERVAL_CHECK_ECHO_KEEP_ALIVE * 1000;
        if (sleepTime > 0) {
            Thread t = new Thread(() -> {
                try {
                    while (!CommunicatorImpl.this.isInterrupted()) {
                        Thread.sleep(sleepTime);

                        LogU.d(TAG, "System.currentTimeMillis() - lastEchoKeepAliveRcvTime = " + (System.currentTimeMillis() - lastEchoKeepAliveRcvTime));

                        if (lastEchoKeepAliveRcvTime > 0 && System.currentTimeMillis() - lastEchoKeepAliveRcvTime > BuildConfig.INTERVAL_CHECK_ECHO_KEEP_ALIVE * 1000 * 2) {
                            disconnect();

                        } else {
                            request(RequestType.ECHO_KEEP_ALIVE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            t.setDaemon(true);
            t.start();
        }
    }

    private void send(String message) throws IOException {
        LogU.d(TAG, "send: " + message);
        send(MessageGenerator.generateMessagePacket(message));
    }

    private void send(byte[] packet) throws IOException {
        dataOutputStream.write(packet);
        dataOutputStream.flush();
    }

    private byte[] readByteArray(int dataLength) throws IOException {
        byte[] receiveData = new byte[dataLength];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        int read;
        while ((read = dataInputStream.read(receiveData, 0, dataLength)) != -1) {
//            LogU.d(TAG, "while read : " + read);
            byteArrayOutputStream.write(receiveData, 0, read);
            dataLength -= read;
//            LogU.d(TAG, "while remain dataLength : " + dataLength);
            if (dataLength <= 0) {
                break;
            }
        }

//        while ((read = dataInputStream.read(receiveData)) != -1) {
//            LogU.d(TAG, "while read : " + read);
//            byteArrayOutputStream.write(receiveData, 0, read);
//            byteArrayOutputStream.flush();
//        }

        byteArrayOutputStream.flush();
        byteArrayOutputStream.close();

        return byteArrayOutputStream.toByteArray();
    }

    private int readHeaderPacketType() throws IOException {
        byte[] readByteArray = readByteArray(HEADER_PACKET_TYPE_SIZE);
        return MessageGenerator.getBigEndian(readByteArray);
    }

    private int readHeaderBodyLength() throws IOException {
        byte[] readByteArray = readByteArray(HEADER_BODY_LENGTH_SIZE);
        return MessageGenerator.getBigEndian(readByteArray);
    }

    private String read() throws IOException {
        LogU.d(TAG, "read() ");

        int packetType = readHeaderPacketType();
        LogU.d(TAG, "Recv packetType : " + packetType);

        String data = null;
        Charset charset = MessageGenerator.getCharset(packetType);

        if (charset != null) {

            int dataLength = readHeaderBodyLength();
            LogU.d(TAG, "Recv dataLength : " + dataLength);

            data = new String(readByteArray(dataLength), charset);
            LogU.d(TAG, "Recv Text size : " + data.length());
            LogU.d(TAG, "Recv Text (is not full text. limit 40KByte) : " + data);

//            String msg = data;
//            while (msg.length() > 0) {
//                if (msg.length() > 3000) {
//                    LogU.d(TAG, msg.substring(0, 3000));
//                    msg = msg.substring(3000);
//                } else {
//                    LogU.d(TAG, msg);
//                    msg = "";
//                }
//            }
        }

        if (packetType == PACKET_TYPE_ECHO_KEEP_ALIVE) {
            LogU.d(TAG, "Recv echoKeepAlive");
            lastEchoKeepAliveRcvTime = System.currentTimeMillis();
            return read();
        }

        return data;
    }

    private static final int REQUEST = 0;
    private static final int RESPONSE = 1;

    public class RequestHandler extends Handler {

        private RequestHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            RequestType requestType = (RequestType) msg.obj;
            LogU.d(TAG, "RequestHandler RequestType = " + requestType);

            try {
                if (requestType == RequestType.ECHO_KEEP_ALIVE) {
                    send(MessageGenerator.generateEchoKeepAlivePacket(MessageGenerator.generateSessionKey(encryptKey, session)));
                } else {
                    send(MessageGenerator.getRequestMessage(MessageGenerator.generateSessionKey(encryptKey, session), requestType));
                    if (RequestType.EVENT_HISTORY == requestType) {
                        Thread.sleep(500);
                    }
                }

            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
//                disconnect();
                if (socket != null) {
                    try {
                        socket.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
    }

    public class ResponseHandler extends Handler {

        private ResponseHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            String xml = (String) msg.obj;
            CommunicatorEvent communicatorEvent = MessageProcessor.processMessage(device, xml);

            if (communicatorEvent != null && communicatorEvent.getResponseType() != ResponseType.NULL) {
                LogU.d(TAG, "ResponseHandler ResponseType = " + communicatorEvent.getResponseType());
                onReceiveEvent(communicatorEvent);

            } else {
                LogU.d(TAG, "ResponseHandler Packet Error");
            }

        }
    }

    @Override
    public synchronized void disconnect() {
        LogU.d(TAG, "disconnect()");

        isDisconnect = true;

        interrupt();

        if (socket != null) {
            onReceiveEvent(ResponseType.DISCONNECTED);
        }

        if (requestHandler != null) {
            try {
                requestHandler.removeMessages(REQUEST);
                requestHandler.getLooper().quitSafely();
            } catch (Exception e) {
                e.printStackTrace();
            }
            requestHandler = null;
        }

        if (responseHandler != null) {
            try {
                responseHandler.removeMessages(RESPONSE);
                responseHandler.getLooper().quitSafely();
            } catch (Exception e) {
                e.printStackTrace();
            }
            responseHandler = null;
        }

        if (dataOutputStream != null) {
            try {
                dataOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            dataOutputStream = null;
        }

        if (dataInputStream != null) {
            try {
                dataInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            dataInputStream = null;
        }

        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            socket = null;
        }

    }

    private void onReceiveEvent(ResponseType responseType) {
        LogU.d(TAG, "onReceiveEvent() " + responseType);

        if (responseType == ResponseType.CONNECTED) {
            checkEchoKeepAlive();
        }

        onReceiveEvent(new CommunicatorEvent(responseType, device, null));
    }

    private void onReceiveEvent(CommunicatorEvent communicatorEvent) {
        if (communicatorEventListener != null) {
            communicatorEventListener.onCommunicatorEvent(communicatorEvent);
        }
    }

    @Override
    public void request(RequestType responseType) {
        if (device.isConnected() && requestHandler != null) {
            LogU.d(TAG, "request() " + responseType.getString());
            Message msg = requestHandler.obtainMessage(REQUEST, responseType);
            requestHandler.sendMessage(msg);
        }
    }

}
